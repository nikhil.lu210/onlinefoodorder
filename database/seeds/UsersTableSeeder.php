<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Super Admin',
            'username' => 'super_admin',
            'email' => 'super_admin@mail.com',
            'password' => bcrypt('12345678'),
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('12345678'),
        ]);

        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Customer',
            'username' => 'customer',
            'email' => 'customer@mail.com',
            'password' => bcrypt('12345678'),
        ]);
    }
}
