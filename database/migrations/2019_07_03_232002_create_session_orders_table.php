<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('session_id');
            $table->json('orders');
            $table->json('setedCoupon')->nullable();
            $table->decimal('delivery_price', 9, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_orders');

        Schema::table("session_orders", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
