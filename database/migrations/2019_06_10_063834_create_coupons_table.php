<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('code', 20)->unique();
            $table->decimal('discount', 9,2);
            $table->decimal('min_purchase', 9,2);
            $table->tinyInteger('uses_limit')->default(1);
            $table->date('exp_date');
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');

        Schema::table("coupons", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
