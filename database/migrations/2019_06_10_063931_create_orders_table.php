<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('country', 100);
            $table->string('city', 100);
            $table->string('area', 100);
            $table->string('phone', 30);
            $table->text('address');

            $table->tinyInteger('delivery_type');
            $table->tinyInteger('payment_type');
            $table->string('payment_recept_url')->nullable();

            $table->tinyInteger('order_type');
            $table->date("order_date");
            $table->time("order_time");


            $table->tinyInteger('order_status');

            $table->json('order_list');
            $table->decimal('original_price', 9, 2);
            $table->decimal('total_discount', 9, 2);
            $table->decimal('reduced_price', 9, 2);
            $table->bigInteger('copon_id')->unsigned()->nullable();

            $table->bigInteger('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('delivery_boy_id')->unsigned()->nullable();
            $table->foreign('delivery_boy_id')->references('id')->on('delivery_boys')->onDelete('cascade');

            $table->text('order_note')->nullable();
            $table->text('cancel_note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');

        Schema::table("orders", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
