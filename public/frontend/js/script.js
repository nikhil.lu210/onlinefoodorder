$(document).ready(function() {

    if ($(this).scrollTop() > 4) {
        $('.navbar').addClass('background');
    } else {
        $('.navbar').removeClass('background');
    }

    // Transition effect for navbar
    $(window).scroll(function() {
        // checks if window is scrolled more than 5px, adds/removes solid class
        if ($(this).scrollTop() > 4) {
            $('.navbar').addClass('background');
        } else {
            $('.navbar').removeClass('background');
        }
    });


    // Category Nav Link Activation
    $('.nav-tabs .nav-link').click(function(){
        $('.nav-tabs .nav-link').removeClass('active');
        $(this).addClass('active');
    })


    // Cart Part
    $('input').on('click',function () {
        var pickup, delivery, cashon, digital, active;

        pickup = $('.pickup-input');
        delivery = $('.delivery-input');


        cashon = $('.cashon-input');
        digital = $('.digital-input');
        active = $('.btn-cashon-vcard');

        // Cashon ot Digital
        if ((cashon.is(':checked') || digital.is(':checked')) && (pickup.is(':checked') || delivery.is(':checked'))) {
            active.removeClass('d-none');
            $('.btn-display').addClass('d-none');
        }else {
            active.addClass('d-none');
            $('.btn-display').removeClass('d-none');
        }

    });


    // Food Search
    $(document).ready(function(){
        $("#searchInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#menuTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });


    // Quantity Input
    $('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    $('.quantity').each(function() {
      var spinner = $(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });


    // Checkbox
    // var disBtn = $('.quantity-cart-td .quantity .quantity-nav');
    $('.quantity-nav').addClass('d-none');
    $('input').on('click',function () {
        var row = '#' + $(this).parent().parent().parent().attr("id");
        var item_quantity = '#' + $(row).children("td.modal-quantity-cart-td").attr("id");

        var status = $(row + ' .status');

        var dis_fld = $(row + ' ' + item_quantity + ' .extra-item-quantity');
        var dis_btn = $(row + ' ' + item_quantity + ' .quantity .quantity-nav');

        if (status.is(':checked')) {
            dis_fld.removeAttr('disabled', 'disabled');
            dis_btn.removeClass('d-none');
        } else {
            dis_fld.attr('disabled', 'disabled');
            dis_btn.addClass('d-none');
        }
    });

});

// ================== ajax for set coupon and update and mainly home page ======================////

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

// ====================== this is for food add to cart ====================
$("button.btn-add-food").click(function(e){
    e.preventDefault();
    $('.modal').modal('hide');

    var parent  = '#'+$(this).parent().parent().parent().parent().attr('id');
    var id = $(parent).children('input').val();
    var form = $(parent);
    var formData = $(form).serialize();

    $.ajax({
        method: 'POST',
        url: 'cart/setFoodCookies/'+id ,
        data: formData,
        datatype: "JSON",
        success: function(data) {
            generateCard(data);
        },
        error: function() {
            console.log("cookies not updated");
        }
    });

});
//============== close add to food  cart  ===================


// ====================== this is for package add to cart ====================
$("button.btn-add-package").click(function(e){
    e.preventDefault();
    $('.modal').modal('hide');


    var parent  = '#'+$(this).parent().parent().parent().parent().attr('id');
    var id = $(parent).children('input').val();
    var form = $(parent);
    var formData = $(form).serialize();

    $.ajax({
        method: 'POST',
        url: 'cart/setPackageCookies/'+id ,
        data: formData,
        datatype: "JSON",
        success: function(data) {
            generateCard(data);
        },
        error: function() {
            console.log("do not add to cart");
        }
    });

});
//============== close package add to cart  ===================


// ==================== update cart ======================
$("button.btn-update-cart").click(function(e){
    e.preventDefault();
    $('.modal').modal('hide');

    var parent  = '#'+$(this).parent().parent().parent().parent().attr('id');
    var id = $(parent).children('input').val();
    var form = $(parent);
    var formData = $(form).serialize();
    $.ajax({
        method: 'POST',
        url: 'cart/updateOrderCookies/'+id ,
        data: formData,
        datatype: "JSON",
        success: function(data) {
            generateCard(data);
        },
        error: function() {
            console.log("do not add to cart");
        }
    });

});

// ==================== update cart ======================

// ================= destroy item from cookies ==============================
$(".remove-cart-btn button").click(function(e){
    e.preventDefault();
    var address = $(this).attr('id');
    var id = address.substring(12, address.length);

    $.ajax({
        method: 'GET',
        url: 'cart/destroyOrderCookies/'+id ,
        datatype: "JSON",
        success: function(data) {
            generateCard(data);
        },
        error: function() {
            console.log("do not destory from cart");
        }
    });

});
// ================= destroy item from cookies ==============================

function jsdelete(id){
    var confirm = ConfirmDelete();
    if(!confirm) return;
    $.ajax({
        method: 'GET',
        url: 'cart/destroyOrderCookies/'+id ,
        datatype: "JSON",
        success: function(data) {
            generateCard(data);
        },
        error: function() {
            console.log("do not destory from cart");
        }
    });
}




function generateCard(data){
    var tr = "";
    var total_price = 0;
    var show_price = 0;

    for(var i=0; i< data.length; i++){
        total_price += data[i].original_price;
        show_price += data[i].price;


        var name = JSON.parse(getFoodPackageName(data[i].id, data[i].food_package));

        tr += "<tr class='h-100'><td class='total-item-td h-100 justify-content-center align-items-center'><div class='total-item'>";
        tr += data[i].quantity + "</div></td>";

        tr += "<td class='food-name-td'><a href='#' class='food-name'  data-toggle='modal' data-target='#order_";
        tr += data[i].id+data[i].food_package + "'>" + name + "</a></td>";
        tr += "<td class='cart-price-td'><div class='cart-price'><h6>$"+ data[i].price.toFixed(2) + "</h6></div></td>";
        tr += "<td class='remove-cart-td'><div class='remove-cart-btn'><button id='remove_cart_"+i+"' class='btn btn-outline-dark btn-sm btn-remove-cart' onclick='jsdelete("+i+");'><i class='fas fa-minus'></i></button></div></td></tr>";
    }

    var cart_items = $('tbody#cart_items');
    cart_items.empty();
    cart_items.append(tr);

    var httotal_price = $('td#total_price strong');
    var httotal_discount = $('td#total_discount strong');
    var after_discount = $('td#after_discount strong');

    httotal_price.html("$ "+show_price.toFixed(2));
    httotal_discount.html("$ 00.0");
    after_discount.html("$ "+show_price.toFixed(2));

    // ============== for small cart ===================
    var cart_items_small = $('tbody#cart_items_small');
    cart_items_small.empty();
    cart_items_small.append(tr);

    var httotal_price_small = $('td#total_price_small strong');
    var httotal_discount_small = $('td#total_discount_small strong');
    var after_discount_small = $('td#after_discount_small strong');

    httotal_price_small.html("$ "+show_price.toFixed(2));
    httotal_discount_small.html("$ 00.0");
    after_discount_small.html("$ "+show_price.toFixed(2));

    var total_orders = $('#total_orders_small');
    total_orders.html(data.length);



}


function getFoodPackageName(id, fo_pa){
    var temp = $.ajax({
        method: 'GET',
        url: 'cart/getFoodPackageName/' + id +'/'+ fo_pa,
        datatype: "Text",
        async: false,
        success: function(data) {
            return(data);
        },
        error: function() {
            console.log("not find");
        }
    });
    return temp.responseText;
}

// ==================== set coupon web =======================

$("button.btn-coupon-web").click(function(e){
    e.preventDefault();
    var form = $("#set-coupon-web");
    var formData = $(form).serialize();
    $("#set-coupon-input-web").val('');
    $.ajax({
        method: 'POST',
        url: 'cart/calculateCoupon' ,
        data: formData,
        datatype: "JSON",
        success: function(data) {
            if(data == 'login') window.location.replace(document.location.origin+'/login');
            if(data[0] == false && data[1] == "no_coupon") alert("This Coupon is not avialable now!!!");
            else if(data[0] == false && data[1] == "min_purchase") alert("You must Take more than "+data[2]+" cost!!!");
            else if(data[0] == false && data[1] == "use_limit") alert("You already cross uses limit for this coupon!!!");
            else generateCartWithCoupon(data);
        },
        error: function() {
            console.log("do not add to cart");
        }
    });

});

// ==================== set coupon web =======================


// ==================== set coupon small cart =======================

$("button.btn-coupon-small").click(function(e){
    e.preventDefault();
    var form = $("#set-coupon-small");
    var formData = $(form).serialize();
    $("#set-coupon-input-small").val('');
    $.ajax({
        method: 'POST',
        url: 'cart/calculateCoupon' ,
        data: formData,
        datatype: "JSON",
        success: function(data) {
            if(data == 'login') window.location.replace(document.location.origin+'/login');
            if(data[0] == false && data[1] == "no_coupon") alert("This Coupon is not avialable now!!!");
            else if(data[0] == false && data[1] == "min_purchase") alert("You must Take more than "+data[2]+" cost!!!");
            else if(data[0] == false && data[1] == "use_limit") alert("You already cross uses limit for this coupon!!!");
            else generateCartWithCoupon(data);
        },
        error: function() {
            console.log("do not add to cart");
        }
    });

});

// ==================== set coupon small cart =======================

$.ajax({
    method: 'GET',
    url: '/destroyoldorder',
    datatype: "JSON",
    success: function(data) {
    },
    error: function() {
        console.log("do not add to cart");
    }
});



function generateCartWithCoupon(val){
    var tr = "";
    var total_price = 0;
    var show_price = 0;

    var data = val[1];

    for(var i=0; i< data.length; i++){
        total_price += data[i].original_price;
        show_price += data[i].price;


        var name = JSON.parse(getFoodPackageName(data[i].id, data[i].food_package));

        tr += "<tr class='h-100'><td class='total-item-td h-100 justify-content-center align-items-center'><div class='total-item'>";
        tr += data[i].quantity + "</div></td>";

        tr += "<td class='food-name-td'><a href='#' class='food-name'  data-toggle='modal' data-target='#order_";
        tr += data[i].id+data[i].food_package + "'>" + name + "</a></td>";
        tr += "<td class='cart-price-td'><div class='cart-price'><h6>$"+ data[i].original_price.toFixed(2) + "</h6></div></td>";
        tr += "<td class='remove-cart-td'><div class='remove-cart-btn'><button id='remove_cart_"+i+"' class='btn btn-outline-dark btn-sm btn-remove-cart' onclick='jsdelete("+i+");'><i class='fas fa-minus'></i></button></div></td></tr>";
    }

    var cart_items = $('tbody#cart_items');
    cart_items.empty();
    cart_items.append(tr);

    var httotal_price = $('td#total_price strong');
    var httotal_discount = $('td#total_discount strong');
    var after_discount = $('td#after_discount strong');
    httotal_price.html("$ "+ total_price.toFixed(2));
    httotal_discount.html("$ "+ val[2].totalDiscount.toFixed(2));
    after_discount.html("$ "+val[2].reducedPrice.toFixed(2));

    // ============== for small cart ===================
    var cart_items_small = $('tbody#cart_items_small');
    cart_items_small.empty();
    cart_items_small.append(tr);

    var httotal_price_small = $('td#total_price_small strong');
    var httotal_discount_small = $('td#total_discount_small strong');
    var after_discount_small = $('td#after_discount_small strong');

    httotal_price_small.html("$ "+ total_price.toFixed(2));
    httotal_discount_small.html("$ "+ val[2].totalDiscount.toFixed(2));
    after_discount_small.html("$ "+val[2].reducedPrice.toFixed(2));

    var total_orders = $('#total_orders_small');
    total_orders.html(data.length);


}

