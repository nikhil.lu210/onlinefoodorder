$(document).ready(function($) {
    function mediaSize() {
        // var wSize = $(window).width();
        /* Set the matchMedia */

        /*===============================================
        ==========< Device Maximum 575.98px >============
        =================================================*/
        if (window.matchMedia('(max-width: 575.98px)').matches) {
            // Category
            $('.categories-part').parent().addClass('d-none');

            // Side Category Bar
            $('.btn-category').on('click', function(){
                $('#mySidenav').css('width', '250px');
            });
            $('.closebtn, .clsbtn').on('click', function(){
                $('#mySidenav').css('width', '0px');
            });
            $('.sidenav .clsbtn').click(function(){
                $('.sidenav .clsbtn').removeClass('active');
                $(this).addClass('active');
            })

            // Small Device Cart
            $('.cart-part').parent().addClass('d-none');
            $('.cart-btn-div').addClass('d-none');
            $(window).scroll(function() {
                // checks if window is scrolled more than 5px, adds/removes solid class
                if ($(this).scrollTop() > 200) {
                    $('.cart-btn-div').removeClass('d-none');
                } else {
                    $('.cart-btn-div').addClass('d-none');
                }
            });

        }


        /*=============================================================
        ==========< Device Size Between 576px to 767.98px >============
        =============================================================*/
        else if (window.matchMedia('(min-width: 576px)' && '(max-width: 767.98px)').matches) {

            // Small Device Cart
            $('.cart-part').parent().addClass('d-none');

        }


        /*=============================================================
        ==========< Device Size Between 768px to 991.98px >============
        ===============================================================*/
        else if (window.matchMedia('(min-width: 768px)' && '(max-width: 991.98px)').matches) {

            // Category
            $('#mySidenav').parent().addClass('d-none');
            $('.categories-part').parent().removeClass('col-md-3').addClass('col-md-4');
            $('.menus-part').parent().removeClass('col-md-6').addClass('col-md-8');

            $('.order-list').parent().removeClass('col-md-6').addClass('col-md-12');

            $('.payment-part.card .address-details , .payment-part.cash .address-details').parent().parent().parent().parent().removeClass('col-md-6').addClass('col-md-12');
            // $('.payment-part.cash .address-details').parent().parent().parent().parent().removeClass('col-md-6').addClass('col-md-12');

            // Small Device Cart
            $('.cart-part').parent().addClass('d-none');
            $('.cart-btn-div').addClass('d-none');
            $(window).scroll(function() {
                // checks if window is scrolled more than 5px, adds/removes solid class
                if ($(this).scrollTop() > 200) {
                    $('.cart-btn-div').removeClass('d-none');
                } else {
                    $('.cart-btn-div').addClass('d-none');
                }
            });

        }


        /*==============================================================
        ==========< Device Size Between 992px to 1199.98px >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 992px)' && '(max-width: 1199.98px)').matches) {

            // Category
            $('#mySidenav').parent().addClass('d-none');

            // Cart Small Device
            $('.cart-btn-div').addClass('d-none');

        }


        /*================================================================
        ==========< Device Size Between 1200px to infinty :p >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 1200px)').matches) {

            // Category
            $('#mySidenav').parent().addClass('d-none');
            $('.cart-btn-div').addClass('d-none');

        }


        /*==========================================================
        ==========< Device Size if those are not match >============
        ==========================================================*/
        else {

            // Category
            $('#mySidenav').parent().addClass('d-none');
            $('.cart-btn-div').addClass('d-none');

        }
    };

    /* Call the function */
    mediaSize();
    /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);

});
