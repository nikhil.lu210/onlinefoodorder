<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function role(){
        return $this->belongsTo('App\Model\Role');
    }

    public function usedCoupons(){
        return $this->hasMany('App\Model\Backend\UsedCoupon\UsedCoupon');
    }

    public function orders(){
        return $this->hasMany('App\Model\Backend\Order\Order', 'user_id', 'id');
    }
    public function admin_orders(){
        return $this->hasMany('App\Model\Backend\Order\Order', 'admin_id', 'id');
    }


}
