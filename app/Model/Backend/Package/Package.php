<?php

namespace App\Model\Backend\Package;

use Illuminate\Database\Eloquent\Model;

// Traits
use App\Model\Backend\Package\Traits\Relations;
use App\Model\Backend\Package\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [ 'name' ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'id' => 'int',
        'extra_food_list' => 'array'
   ];
}
