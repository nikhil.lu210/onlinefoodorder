<?php

namespace App\Model\Backend\Package\Traits;

// Model
use App\Model\Backend\Package\Package;

trait Relations
{
    public function offers()
    {
        return $this->belongsToMany('App\Model\Backend\Offer\Offer');
    }
}
