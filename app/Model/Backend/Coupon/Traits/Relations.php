<?php

namespace App\Model\Backend\Coupon\Traits;

// Model
use App\Model\Backend\Coupon\Coupon;

trait Relations
{
    public function used_coupons()
    {
        return $this->hasMany('App\Model\Backend\UsedCoupon\UsedCoupon');
    }
}
