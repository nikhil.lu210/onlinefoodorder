<?php

namespace App\Model\Backend\Food\Traits;

// Model
use App\Model\Backend\Food\Food;

trait Relations
{
    public function category()
    {
        return $this->belongsTo('App\Model\Backend\Category\Category', 'cat_id', 'id');
    }

    public function offers()
    {
        return $this->belongsToMany('App\Model\Backend\Offer\Offer');
    }
}
