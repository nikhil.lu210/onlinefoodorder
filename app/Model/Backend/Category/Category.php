<?php

namespace App\Model\Backend\Category;

use Illuminate\Database\Eloquent\Model;

// Traits
use App\Model\Backend\Category\Traits\Relations;
use App\Model\Backend\Category\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name' ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
