<?php

namespace App\Model\Backend\Category\Traits;

// Model
use App\Model\Backend\Category\Category;

trait Relations
{
    public function foods()
    {
        return $this->hasMany('App\Model\Backend\Food\Food');
    }
}
