<?php

namespace App\Model\Backend\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

// Traits
use App\Model\Backend\Order\Traits\Relations;
use App\Model\Backend\Order\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [ 'name' ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
