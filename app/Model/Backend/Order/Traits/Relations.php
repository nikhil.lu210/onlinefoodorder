<?php

namespace App\Model\Backend\Order\Traits;

// Model
use App\Model\Backend\Order\Order;

trait Relations
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id', 'id');
    }

    public function deliveryBoy(){
        return $this->belongsTo('App\Model\Backend\DeliveryBoy\DeliveryBoy');
    }
}
