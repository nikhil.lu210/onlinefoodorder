<?php

namespace App\Model\Backend\UsedCoupon;

use Illuminate\Database\Eloquent\Model;

// Traits
use App\Model\Backend\UsedCoupon\Traits\Relations;
use App\Model\Backend\UsedCoupon\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsedCoupon extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [ 'name' ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
