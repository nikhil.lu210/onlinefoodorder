<?php

namespace App\Model\Backend\UsedCoupon\Traits;

// Model
use App\Model\Backend\UsedCoupon\UsedCoupon;

trait Relations
{
    public function coupon()
    {
        return $this->belongsTo('App\Model\Backend\Coupon\Coupon');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
