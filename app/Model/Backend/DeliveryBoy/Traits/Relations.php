<?php

namespace App\Model\Backend\DeliveryBoy\Traits;

// Model
use App\Model\Backend\DeliveryBoy\DeliveryBoy;

trait Relations
{
    public function orders()
    {
        return $this->hasMany('App\Model\Backend\Order\Order');
    }
}
