<?php

namespace App\Model\Backend\Offer;

use Illuminate\Database\Eloquent\Model;

// Traits
use App\Model\Backend\Offer\Traits\Relations;
use App\Model\Backend\Offer\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [ 'name' ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'id' => 'int',
        'food_ids' => 'array',
        'package_ids' => 'array',
   ];
}
