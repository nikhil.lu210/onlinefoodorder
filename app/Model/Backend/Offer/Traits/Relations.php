<?php

namespace App\Model\Backend\Offer\Traits;

// Model
use App\Model\Backend\Offer\Offer;

trait Relations
{
    public function foods()
    {
        return $this->belongsToMany('App\Model\Backend\Food\Food');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Model\Backend\Package\Package');
    }
}
