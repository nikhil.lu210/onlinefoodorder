<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'role_id',
        'name',
        'slug'
    ];

    public function users(){
        return $this->hasMany('App\User');
    }
}
