<?php

namespace App\Http\Controllers\Admin\Offer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Coupon\Coupon;

class CouponController extends Controller
{

    private $coupon;

    public function __construct(){
        $this->coupon = new Coupon();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = $this->coupon->all();
        return view('backend.admin.offer.coupon.index')->withCoupons($coupons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.offer.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'name'          =>      'required | string | max:191',
            'code'          =>      'required | string | max:20 | unique:coupons',
            'discount'      =>      'required | between:0,99.99 | numeric | max:100',
            'min_purchase'  =>      'required | between:0,99999.99 | numeric | max:10000',
            'uses_limit'    =>      'required | integer',
            'exp_date'      =>      'required | date | date_format:Y-m-d',
        ]);

        if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $coupon = new Coupon(); //Assigning new row

        $coupon->name           =       $request->name;
        $coupon->code           =       $request->code;
        $coupon->discount       =       $request->discount;
        $coupon->min_purchase   =       $request->min_purchase;
        $coupon->uses_limit     =       $request->uses_limit;
        $coupon->exp_date       =       $request->exp_date;
        $coupon->note           =       $request->note;

        $coupon->save();

        return redirect()->route('admin.offer.all.coupon');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coupon = $this->coupon->find($id);
        return view('backend.admin.offer.coupon.show')->withCoupon($coupon);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          =>      'required | string | max:191',
            'code'          =>      'required | string | max:20 ',
            'discount'      =>      'required | between:0,99.99 | numeric | max:100',
            'min_purchase'  =>      'required | between:0,99999.99 | numeric | max:10000',
            'uses_limit'    =>      'required | integer',
            'exp_date'      =>      'required | date | date_format:Y-m-d',
        ]);

        if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }
        $coupon = $this->coupon->find($id);
        if($coupon->code != $request->code){
            $this->validate($request, [
                'code'          =>      'unique:coupons',
            ]);
        }



        $coupon->name           =       $request->name;

        $coupon->code           =       $request->code;
        $coupon->discount       =       $request->discount;
        $coupon->min_purchase   =       $request->min_purchase;
        $coupon->uses_limit     =       $request->uses_limit;
        $coupon->exp_date       =       $request->exp_date;
        $coupon->note           =       $request->note;

        $coupon->save();

        return redirect()->route('admin.offer.all.coupon');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = $this->coupon->find($id)->delete();

        return redirect()->back();
    }
}
