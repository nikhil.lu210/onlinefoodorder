<?php

namespace App\Http\Controllers\Admin\Offer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Offer\Offer;
use App\Model\Backend\Food\Food;
use App\Model\Backend\Package\Package;

class OfferController extends Controller
{
    private $offer, $food, $package;

    public function __construct(){
        $this->offer = new Offer();
        $this->food = new Food();
        $this->package = new Package();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = $this->offer->all();
        return view('backend.admin.offer.offer.index')->withOffers($offers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $foods = $this->food->select('id', 'name', 'price')->get();
        $packages = $this->package->select('id', 'name', 'price')->get();
        return view('backend.admin.offer.offer.create')
                    ->withFoods($foods)
                    ->withPackages($packages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name'          =>      'required | string | max:191',
            'discount'      =>      'required | between:0,99.99 | numeric | max:100',
            'exp_date'      =>      'required | date | date_format:Y-m-d',
        ]);


        if(isset($request->food_ids[0])){
            $this->validate($request,[
                'food_ids' => 'required|array',
                'food_ids.*' => 'integer',
            ]);
        }

        if(isset($request->package_ids[0])){
            $this->validate($request,[
                'package_ids' => 'required|array',
                'package_ids.*' => 'integer',
            ]);
        }

        if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $offer = new Offer(); //Assigning new row

        $offer->name           =       $request->name;
        $offer->discount       =       $request->discount;

        if(isset($request->food_ids[0])) $offer->food_ids = $request->food_ids;
        if(isset($request->package_ids[0])) $offer->package_ids = $request->package_ids;

        $offer->exp_date       =       $request->exp_date;
        $offer->note           =       $request->note;

        $offer->save();

        return redirect()->route('admin.offer.all.offer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foods = $this->food->select('id', 'name')->get();
        $packages = $this->package->select('id', 'name')->get();
        $offer = $this->offer->find($id);
        return view('backend.admin.offer.offer.show')
                    ->withFoods($foods)
                    ->withPackages($packages)
                    ->withOffer($offer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          =>      'required | string | max:191',
            'discount'      =>      'required | between:0,99.99 | numeric | max:100',
            'exp_date'      =>      'required | date | date_format:Y-m-d',
        ]);


        if(isset($request->food_ids[0])){
            $this->validate($request,[
                'food_ids' => 'required|array',
                'food_ids.*' => 'integer',
            ]);
        }

        if(isset($request->package_ids[0])){
            $this->validate($request,[
                'package_ids' => 'required|array',
                'package_ids.*' => 'integer',
            ]);
        }

        if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $offer = $this->offer->find($id);

        $offer->name           =       $request->name;
        $offer->discount       =       $request->discount;

        if(isset($request->food_ids[0])) $offer->food_ids = $request->food_ids;
        if(isset($request->package_ids[0])) $offer->package_ids = $request->package_ids;

        $offer->exp_date       =       $request->exp_date;
        $offer->note           =       $request->note;

        $offer->save();

        return redirect()->route('admin.offer.all.offer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = $this->offer->find($id)->delete();

        return redirect()->back();
    }
}
