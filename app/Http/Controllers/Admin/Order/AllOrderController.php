<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Order\Order;
use App\Model\Backend\DeliveryBoy\DeliveryBoy;
use Auth;
use App\User;

class AllOrderController extends Controller
{

    private $order, $deliveryBoy;

    public function __construct(){
        $this->order = new Order();
        $this->deliveryBoy = new DeliveryBoy();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->order->with(['user' => function($query){
                    $query->select('id', 'name');
                }])->get();
        return view('backend.admin.record.all_order.index')
                    ->withOrders($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->order->with(['user' => function($query){
            $query->select('id', 'name');
        }, 'deliveryBoy' => function($query){
            $query->select('id', 'name');
        }, 'admin' => function($query){
            $query->select('id', 'name');
        }])->find($id);

        $boys = $this->deliveryBoy->all();

        return view('backend.admin.record.all_order.show')
                    ->withOrder($order)
                    ->withBoys($boys);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // Confirmation
    public function confirm(Request $request, $id){
        // dd($request);
        $this->validate($request, [
            'order_note' => 'string | nullable',
        ]);

        $order = $this->order->find($id);

        $order->delivery_boy_id = $request->delivery_boy;
        $order->order_note = $request->order_note;
        $order->order_status = 1;
        $order->admin_id = Auth::user()->id;

        $order->save();

        return redirect()->back();
    }

    // Cancellation
    public function cancel(Request $request, $id){
        // dd($request);
        $this->validate($request, [
            'cancel_note' => 'string | nullable',
        ]);

        $cancel = $this->order->find($id);

        $cancel->cancel_note = $request->cancel_note;
        $cancel->order_status = -1;
        $cancel->admin_id = Auth::user()->id;

        $cancel->save();

        return redirect()->back();
    }

    // Complete
    public function complete($id){

        $complete = $this->order->find($id);

        $complete->order_status = 2;

        $complete->save();

        return redirect()->back();
    }
}
