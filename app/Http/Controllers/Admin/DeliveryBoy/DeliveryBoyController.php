<?php

namespace App\Http\Controllers\Admin\DeliveryBoy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\DeliveryBoy\DeliveryBoy;
use Storage;

class DeliveryBoyController extends Controller
{

    private $deliveryBoy;

    public function __construct(){
        $this->deliveryBoy = New DeliveryBoy();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveryBoys = $this->deliveryBoy->select('id', 'avatar', 'name', 'mobile', 'email')->get();
        return view('backend.admin.delivery_boy.index')->withDeliveryBoys($deliveryBoys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.delivery_boy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          =>  'required | string | max:100',
            'mobile'        =>  'required | string | max:30',
        ]);

        if(isset($request->email)){
            $this->validate($request, [
                'email'          =>  'string | max:100',
            ]);
        }

        if(isset($request->address)){
            $this->validate($request, [
                'address'          =>  'string',
            ]);
        }

        if(isset($request->note)){
            $this->validate($request, [
                'note'          =>  'string',
            ]);
        }

        if($request -> hasFile('avatar')){
            $avatar = $request->file('avatar');
            $mime = $avatar->getMimeType();

            if ($mime == "image/jpeg" || $mime == "image/png" || $mime == "image/svg+xml") {
                $this->validate($request,array(
                    'avatar'    =>  'image|mimes:jpeg,png,jpg,svg|max:1024',
                ));
            }
        }

        $deliveryBoy = new DeliveryBoy();

        $deliveryBoy->name      =   $request->name;
        $deliveryBoy->mobile    =   $request->mobile;

        if(isset($request->email))
            $deliveryBoy->email = $request->email;

        if(isset($request->address))
            $deliveryBoy->address = $request->address;

        if(isset($request->note))
            $deliveryBoy->note = $request->note;

        if($request -> hasFile('avatar'))
            $deliveryBoy->avatar = $request->file('avatar')->store('delivery_boy', 'public');

        $deliveryBoy->save();

        return redirect()->route('admin.delivery.all.delivery_boy');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $boy = $this->deliveryBoy->find($id);
        return view('backend.admin.delivery_boy.show')->withBoy($boy);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          =>  'required | string | max:100',
            'mobile'        =>  'required | string | max:30',
        ]);

        if(isset($request->email)){
            $this->validate($request, [
                'email'          =>  'string | max:100',
            ]);
        }

        if(isset($request->address)){
            $this->validate($request, [
                'address'          =>  'string',
            ]);
        }

        if(isset($request->note)){
            $this->validate($request, [
                'note'          =>  'string',
            ]);
        }

        if($request -> hasFile('avatar')){
            $avatar = $request->file('avatar');
            $mime = $avatar->getMimeType();

            if ($mime == "image/jpeg" || $mime == "image/png" || $mime == "image/svg+xml") {
                $this->validate($request,array(
                    'avatar'    =>  'image|mimes:jpeg,png,jpg,svg|max:1024',
                ));
            }
        }

        $deliveryBoy = $this->deliveryBoy->find($id);

        $deliveryBoy->name      =   $request->name;
        $deliveryBoy->mobile    =   $request->mobile;

        if(isset($request->email))
            $deliveryBoy->email = $request->email;

        if(isset($request->address))
            $deliveryBoy->address = $request->address;

        if(isset($request->note))
            $deliveryBoy->note = $request->note;

        if($request -> hasFile('avatar')){
            unlink(storage_path('app/public/'.$deliveryBoy->avatar));
            $deliveryBoy->avatar = $request->file('avatar')->store('delivery_boy', 'public');
        }


        $deliveryBoy->save();

        return redirect()->route('admin.delivery.all.delivery_boy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boy = $this->deliveryBoy->find($id)->delete();

        return redirect()->back();
    }
}
