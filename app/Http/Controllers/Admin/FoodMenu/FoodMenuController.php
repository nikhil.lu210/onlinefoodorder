<?php

namespace App\Http\Controllers\Admin\FoodMenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\Food\Food;
use App\Model\Backend\Category\Category;

class FoodMenuController extends Controller
{
    private $food, $category;

    public function __construct(){

        $this->food = new Food();
        $this->category = new Category();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $foods = $this->food->all();
        $foods = $this->food->with(['category' => function($query){
                    $query->select('id', 'name');
                }])->orderBy('id', 'asc')->get();
        // dd($foods);
        return view('backend.admin.food.food_menu.index')->withFoods($foods);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->select('id', 'name')->get();
        $foods = $this->food->select('id', 'name', 'price')->get();

        return view('backend.admin.food.food_menu.create')
                ->withCategories($categories)
                ->withFoods($foods);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name'      =>  'required | max:100 | string',
            'price'     =>  'required | between:0,99999.99 | numeric | max:10000',
            'cat_id'    =>  'required | integer',
        ]);

        if(isset($request->extra_food_list[0])){
            $this->validate($request,[
                'extra_food_list' => 'required|array',
                'extra_food_list.*' => 'integer',
            ]);
        }

       if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $food = new Food();

        $food->name     =   $request->name;
        $food->price    =   $request->price;
        $food->cat_id   =   $request->cat_id;

        if(isset($request->extra_food_list[0])){
            $food->extra_food_list = $request->extra_food_list;
        }

        if(isset($request->note)) $food->note = $request->note;

        $food->save();

        return redirect()->route('admin.food.all.food_menu');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = $this->category->select('id', 'name')->get();
        $food = $this->food->find($id);
        $foods = $this->food->select('id', 'name', 'price')->where('extra_food_list', null)->get();

        return view('backend.admin.food.food_menu.show')
                ->withCategories($categories)
                ->withFoods($foods)
                ->withFood($food);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'      =>  'required | max:100 | string',
            'price'     =>  'required | between:0,99999.99 | numeric | max:10000',
            'cat_id'    =>  'required | integer',
        ]);

        if(isset($request->extra_food_list[0])){
            $this->validate($request,[
                'extra_food_list' => 'required|array',
                'extra_food_list.*' => 'integer',
            ]);
        }

       if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $food = $this->food->find($id);

        $food->name     =   $request->name;
        $food->price    =   $request->price;
        $food->cat_id   =   $request->cat_id;

        if(isset($request->extra_food_list[0])){
            $food->extra_food_list = $request->extra_food_list;
        }

        if(isset($request->note)) $food->note = $request->note;

        $food->save();

        return redirect()->route('admin.food.all.food_menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $food = $this->food->find($id)->delete();

        return redirect()->back();
    }
}
