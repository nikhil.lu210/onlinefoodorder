<?php

namespace App\Http\Controllers\Admin\FoodMenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Backend\Food\Food;
use App\Model\Backend\Package\Package;

class FoodPackageController extends Controller
{
    private $food, $package;

    public function __construct(){

        $this->food = new Food();
        $this->package = new Package();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = $this->package->select('id', 'name', 'package_details', 'price')->get();
        // dd($packages);
        return view('backend.admin.food.package_menu.index')->withPackages($packages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $foods = $this->food->select('id', 'name', 'price')->where('extra_food_list', null)->get();

        return view('backend.admin.food.package_menu.create')
                ->withFoods($foods);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'                  =>  'required | max:100 | string',
            'package_details'       =>  'required | string',
            'price'                 =>  'required | between:0,99.99 | numeric ',
        ]);

        if(isset($request->extra_food_list[0])){
            $this->validate($request,[
                'extra_food_list' => 'required|array',
                'extra_food_list.*' => 'integer',
            ]);
        }

       if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $package = new Package();

        $package->name     =   $request->name;
        $package->package_details     =   $request->package_details;
        $package->price    =   $request->price;

        if(isset($request->extra_food_list)){
            $package->extra_food_list = $request->extra_food_list;
        }

        if(isset($request->note)) $package->note = $request->note;

        $package->save();

        return redirect()->route('admin.food.all.food_package');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foods = $this->food->select('id', 'name', 'price')->where('extra_food_list', null)->get();
        $package = $this->package->find($id);

        return view('backend.admin.food.package_menu.show')
                ->withFoods($foods)
                ->withPackage($package);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'                  =>  'required | max:100 | string',
            'package_details'       =>  'required | string',
            'price'                 =>  'required | between:0,99.99 | numeric ',
        ]);

        if(isset($request->extra_food_list[0])){
            $this->validate($request,[
                'extra_food_list' => 'required|array',
                'extra_food_list.*' => 'integer',
            ]);
        }

       if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        $package = $this->package->find($id);

        $package->name     =   $request->name;
        $package->package_details     =   $request->package_details;
        $package->price    =   $request->price;

        if(isset($request->extra_food_list)){
            $package->extra_food_list = $request->extra_food_list;
        }

        if(isset($request->note)) $package->note = $request->note;

        $package->save();

        return redirect()->route('admin.food.all.food_package');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = $this->package->find($id)->delete();

        return redirect()->back();
    }
}
