<?php

namespace App\Http\Controllers\Admin\FoodMenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Category\Category;
use App\Model\Backend\Food\Food;
use App\Model\Backend\Package\Package;

class CategoryController extends Controller
{
    private $category, $food, $package;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->category = new Category();
        $this->food = new Food();
        $this->package = new Package();
    }

    public function index()
    {
        $categories = $this->category->all();

        return view('backend.admin.food.category.index')
                    ->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.food.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $this->validate($request,[
            'name'  =>  'required | max:30 | string',
        ]);

        if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        // Data Insertion
        $category = new Category(); //makeing new row

        $category->name = $request->name;

        if(isset($request->note)){ $category->note = $request->note; }

        $category->save();

        return redirect()->Route('admin.category.all.category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->category->find($id);
        return view('backend.admin.food.category.show')->withCategory($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation
        $this->validate($request,[
            'name'  =>  'required | max:30 | string',
        ]);

        if(isset($request->note)){
            $this->validate($request,[
                'note'  =>  'string',
            ]);
        }

        // Data Insertion
        $category =  $this->category->find($id);

        $category->name = $request->name;

        if(isset($request->note)){
            $category->note = $request->note;
        }

        $category->save();

        return redirect()->Route('admin.category.all.category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->category->find($id)->delete();

        return redirect()->back();

    }
}
