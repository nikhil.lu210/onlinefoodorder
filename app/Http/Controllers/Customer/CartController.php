<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Food\Food;
use App\Model\Backend\Package\Package;
use App\Model\Backend\Coupon\Coupon;
use App\Model\Backend\UsedCoupon\UsedCoupon;
use App\Model\Backend\Offer\Offer;
use App\Model\Backend\SessionOrder\SessionOrder;
use Cookie;
use Auth;


class CartController extends Controller
{
    private $food, $packagep, $coupon, $usedCoupon, $offer, $sessionOrder;
    private $session_id;

    public function __construct(){
        $this->food = new Food();
        $this->package = new Package();
        $this->coupon = new Coupon();
        $this->usedCoupon = new UsedCoupon();
        $this->offer = new Offer();
        $this->sessionOrder = new SessionOrder();
        $this->session_id = null;
    }



    public function setFoodCookies(Request $request, $id){

        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = array();
        if(!$sessionOrder) {
            $sessionOrder = new SessionOrder();
            $sessionOrder->session_id = $this->session_id;
            $orders = array();

        } else {
            $orders = json_decode($sessionOrder->orders, true);
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        if( $setedCoupon )  $sessionOrder->setedCoupon = null;


        $item_id_list = array_column($orders, 'id');

        $flag = false;
        for($i = 0; $i < sizeof($item_id_list); $i++){
            if($item_id_list[$i] == $id && $orders[$i]["food_package"] == "food"){ $flag = true; break; }
        }
        # check item already set or not.
        if( $flag ){
            #if already set

            foreach($orders as $key => $order){
                if( $orders[$key]["id"] == $id ){   #check which order data is match with updated item

                    $price = $this->getFoodPrice($id) * $request->quantity;
                    $orginal_price = $this->getOriginalFoodPrice($id) * $request->quantity;

                    if( isset($request->ex_quantity) ){
                        foreach ($request->ex_quantity as $i => $ex_quant) {
                            $price += ($this->getFoodPrice($i) * $ex_quant );
                            $orginal_price += ($this->getOriginalFoodPrice($i) * $ex_quant );
                        }
                    }
                    $orders[$key]["quantity"]       =   $request->quantity;
                    $orders[$key]["ex_quantity"]    =   $request->ex_quantity;
                    $orders[$key]["price"]          =   $price;
                    $orders[$key]["original_price"] =   $orginal_price;
                    if( isset($request->note) )         $orders[$key]["note"] = $request->note;

                }
            }

        } else{
            #add new Item in cookies
            $price = $this->getFoodPrice($id) * $request->quantity;
            $orginal_price = $this->getOriginalFoodPrice($id) * $request->quantity;

            if(isset($request->ex_quantity)){
                foreach ($request->ex_quantity as $key => $ex_quant) {
                    $price += ($this->getFoodPrice($key) * $ex_quant );
                    $orginal_price += ($this->getOriginalFoodPrice($key) * $ex_quant );
                }
            }
            $new_order = array(
                'id'            =>  $id,
                'food_package'  =>  'food',
                'quantity'      =>  $request->quantity,
                'ex_quantity'   =>  $request->ex_quantity,
                'note'          =>  $request->note,
                'price'         =>  $price,
                'original_price'=>  $orginal_price,

            );
            $orders[] = $new_order;

        }

        $set_orders = json_encode($orders);

        $sessionOrder->orders = $set_orders;
        $sessionOrder->save();

        return response()->json($orders);
    }



    public function setPackageCookies(Request $request, $id){

        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = array();
        if(!$sessionOrder) {
            $sessionOrder = new SessionOrder();
            $sessionOrder->session_id = $this->session_id;
            $orders = array();

        } else {
            $orders = json_decode($sessionOrder->orders, true);
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        if( $setedCoupon )  $sessionOrder->setedCoupon = null;


        $item_id_list = array_column($orders, 'id');

        $flag = false;
        for($i = 0; $i < sizeof($item_id_list); $i++){
            if($item_id_list[$i] == $id && $orders[$i]["food_package"] == "package"){ $flag = true; break; }
        }
        # check item already set or not.
        if( $flag ){
            #if already set
            foreach($orders as $key => $order){
                if( $orders[$key]["id"] == $id ){   #check which order data is match with updated item

                    $price = $this->getPackagePrice($id) * $request->quantity;
                    $orginal_price = $this->getOriginalPackagePrice($id) * $request->quantity;
                    if(isset($request->ex_quantity)){
                        foreach ($request->ex_quantity as $i => $ex_quant) {
                            $price += ($this->getFoodPrice($i) * $ex_quant );
                            $orginal_price += ($this->getOriginalFoodPrice($i) * $ex_quant );
                        }
                    }
                    $orders[$key]["quantity"]       =   $request->quantity;
                    $orders[$key]["ex_quantity"]    =   $request->ex_quantity;
                    $orders[$key]["price"]          =   $price;
                    $orders[$key]["original_price"] =   $orginal_price;
                    if( isset($request->note) )         $orders[$key]["note"] = $request->note;

                }
            }

        } else{
            #add new Item in cookies
            $price = $this->getPackagePrice($id) * $request->quantity;
            $orginal_price = $this->getOriginalPackagePrice($id) * $request->quantity;

            if(isset($request->ex_quantity)){
                foreach ($request->ex_quantity as $key => $ex_quant) {
                    $price += ($this->getFoodPrice($key) * $ex_quant );
                    $orginal_price += ($this->getOriginalFoodPrice($key) * $ex_quant );
                }
            }
            $new_order = array(
                'id'            =>  $id,
                'food_package'  =>  'package',
                'quantity'      =>  $request->quantity,
                'ex_quantity'   =>  $request->ex_quantity,
                'note'          =>  $request->note,
                'price'         =>  $price,
                'original_price'=>  $orginal_price,

            );
            $orders[] = $new_order;

        }

        $set_orders = json_encode($orders);

        $sessionOrder->orders = $set_orders;
        $sessionOrder->save();

        return response()->json($orders);

    }


    public function updateOrderCookies(Request $request, $id){

        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = array();
        if(!$sessionOrder) {
            $sessionOrder = new SessionOrder();
            $sessionOrder->session_id = $this->session_id;
            $orders = array();

        } else {
            $orders = json_decode($sessionOrder->orders, true);
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        if( $setedCoupon )  $sessionOrder->setedCoupon = null;

        if($orders[$id]["food_package"] == "food"){
            $price = $this->getFoodPrice($orders[$id]["id"]) * $request->quantity;
            $orginal_price = $this->getOriginalFoodPrice($orders[$id]["id"]) * $request->quantity;
        } else{
            $price = $this->getPackagePrice($orders[$id]["id"]) * $request->quantity;
            $orginal_price = $this->getOriginalPackagePrice($orders[$id]["id"]) * $request->quantity;
        }

        if(isset($request->ex_quantity)){
            foreach ($request->ex_quantity as $i => $ex_quant) {
                $price += ($this->getFoodPrice($i) * $ex_quant );
                $orginal_price = $this->getOriginalFoodPrice($i) * $request->quantity;
            }
        }

        $orders[$id]["quantity"]       =   $request->quantity;

        if(isset($request->ex_quantity))
            $orders[$id]["ex_quantity"]    =   $request->ex_quantity;

        $orders[$id]["price"]          =   $price;
        $orders[$id]["original_price"] =   $orginal_price;

        if( isset($request->note) )        $orders[$id]["note"] = $request->note;

        $set_orders = json_encode($orders);

        $sessionOrder->orders = $set_orders;
        $sessionOrder->save();

        return response()->json($orders);

    }


    public function destroyOrderCookies($id){

        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = array();
        if(!$sessionOrder) {
            $sessionOrder = new SessionOrder();
            $sessionOrder->session_id = $this->session_id;
            $orders = array();

        } else {
            $orders = json_decode($sessionOrder->orders, true);
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        if( $setedCoupon )  $sessionOrder->setedCoupon = null;

        array_splice($orders, $id, 1);
        $updated_data = json_encode($orders);

        $sessionOrder->orders = $updated_data;
        $sessionOrder->save();

        return response()->json($orders);
    }


    public function calculateCoupon(Request $request){

        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        if( Auth::check() == false ) return response('login');

        $this->validate($request, array(
            'coupon_code'   =>  'required | string',
        ));

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = array();
        if(!$sessionOrder) {
            $sessionOrder = new SessionOrder();
            $sessionOrder->session_id = $this->session_id;
            $orders = array();

        } else {
            $orders = json_decode($sessionOrder->orders, true);
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        if( $setedCoupon )  $sessionOrder->setedCoupon = null;

        $total = array_sum(array_column($orders, 'original_price'));

        $coupon = $this->coupon->where('code', $request->coupon_code)
                                ->where('exp_date', '>=', date("Y-m-d"))
                                ->first();

        if( !$coupon ) return response()->json(array(false, 'no_coupon'));   #set alert

        $usedCoupon = $this->usedCoupon->where(['user_id' => Auth::user()->id, 'coupon_id' => $coupon->id])->first();

        $cookiesCoupon = null;

        if( !$usedCoupon ){
            $reducedPrice = $total;
            $totalDiscount = 0;

            if( $coupon->min_purchase <= $total ){
                $totalDiscount = ($coupon->discount * $total)/100;
                $reducedPrice = $total - $totalDiscount;
            } else return response()->json(array(false, 'min_purchase', $coupon->min_purchase));

            $cookiesCoupon = array(
                'coupon_id'     =>  $coupon->id,
                'user_id'       =>  Auth::user()->id,
                'uses_limit'    =>  1,
                'reducedPrice'  =>  $reducedPrice,
                'totalDiscount' =>  $totalDiscount,
                'total'         =>  $total,
            );

        } else {

            $reducedPrice = $total;
            $totalDiscount = 0;
            if( $coupon->min_purchase <= $total ){
                $totalDiscount = ($coupon->discount * $total)/100;
                $reducedPrice = $total - $totalDiscount;
            }  else return response()->json(array(false, 'min_purchase', $coupon->min_purchase));

            if($usedCoupon->uses_limit < $coupon->uses_limit){
                $cookiesCoupon = array(
                    'coupon_id'     =>  $coupon->id,
                    'user_id'       =>  Auth::user()->id,
                    'uses_limit'    =>  $usedCoupon->uses_limit++,
                    'reducedPrice'  =>  $reducedPrice,
                    'totalDiscount' =>  $totalDiscount,
                );
            } else return response()->json(array(false, 'use_limit'));
        }


        $setCookiesCoupon = json_encode($cookiesCoupon);


        $sessionOrder->setedCoupon = $setCookiesCoupon;
        $sessionOrder->save();


        return response()->json(array(true, $orders, $cookiesCoupon));

    }



    public function getFoodPrice($id){
        $offer = Offer::where('food_ids','like','%'.$id.'%')
                    ->where('exp_date', '>=', date('Y-m-d'))
                    ->orderBy('updated_at', 'desc')->first();
        if($offer){
            $price  =   number_format($this->food->find($id)->price - ( ($this->food->find($id)->price * $offer->discount)/100 ), 2);
            return $price;
        }
        return $this->food->find($id)->price;
    }

    public function getOriginalFoodPrice($id){
        return $this->food->find($id)->price;
    }


    public function getPackagePrice($id){
        $offer = Offer::where('package_ids','like','%'.$id.'%')
                    ->where('exp_date', '>=', date('Y-m-d'))
                    ->orderBy('updated_at', 'desc')->first();
        if($offer){
            $price  =   number_format($this->package->find($id)->price - ( ($this->package->find($id)->price * $offer->discount)/100 ), 2);
            return $price;
        }
        return $this->package->find($id)->price;
    }

    public function getOriginalPackagePrice($id){
        return $this->package->find($id)->price;
    }

    public function getfoodPackageName($id, $fo_pa){
        if($fo_pa == "food") return response()->json( Food::find($id)->name );
        return response()->json( Package::find($id)->name );
    }
}
