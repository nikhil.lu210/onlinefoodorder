<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

Use App\Model\Backend\Category\Category;
Use App\Model\Backend\Food\Food;
Use App\Model\Backend\Offer\Offer;
Use App\Model\Backend\Package\Package;
use App\Model\Backend\SessionOrder\SessionOrder;
use Cookie;

class HomeController extends Controller
{
    private $category, $food, $package, $offer, $sessionOrder;
    private $session_id;


    public function __construct(Request $request){
        $this->category = new Category();
        $this->food = new Food();
        $this->offer = new Offer();
        $this->package = new Package();
        $this->sessionOrder = new SessionOrder();
        $this->session_id = $this->setunique($request);
    }

    public function setunique($request){

        if(Cookie::get('session_id')) return null;

        $session_id = [
            'id' => $request->header('User-Agent') . now(),
        ];
        $data = json_encode($session_id);
        Cookie::queue('session_id', $data, time() + ( 86400 * 30 ) );  #time() + ( 86400 * 30 );
        return $session_id['id'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = array();
        // dd($sessionOrder);

        if(!$sessionOrder) {
            $sessionOrder = new SessionOrder();
            $sessionOrder->session_id = $this->session_id;
            $orders = array();

        } else {
            $orders = json_decode($sessionOrder->orders, true);
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        $categories = $this->category->select('id', 'name')->get();


        $foods  = $this->food->all();

        $foodsOffer = array();
        foreach ($foods as $key => $food) {
            $offer = Offer::where('food_ids','like','%'.$food->id.'%')
                    ->where('exp_date', '>=', date('Y-m-d'))
                    ->orderBy('updated_at', 'desc')->first();
            if($offer){
                $price  =   number_format($food->price - ( ($food->price * $offer->discount)/100 ), 2);
                $temp = array(
                    'discount'      =>  $offer->discount,
                    'price'         =>  $price,
                );
                $foodsOffer[] = $temp;
            } else {
                $temp = array(
                    'discount'      =>  null,
                    'price'         =>  null,
                );
                $foodsOffer[] = $temp;
            }
        }



        $packages = $this->package->all();


        $packagesOffer = array();
        foreach ($packages as $key => $package) {

            $offer = Offer::where('package_ids','like','%'.$package->id.'%')
                    ->where('exp_date', '>=', date('Y-m-d'))
                    ->orderBy('updated_at', 'desc')->first();
            if($offer){
                $price  =   number_format($package->price - ( ($package->price * $offer->discount)/100 ), 2);
                $temp = array(
                    'discount'      =>  $offer->discount,
                    'price'         =>  $price,
                );
                $packagesOffer[] = $temp;
            } else {
                $temp = array(
                    'discount'      =>  null,
                    'price'         =>  null,
                );
                $packagesOffer[] = $temp;
            }
        }

        return view('frontend.home.index')
                ->withCategories($categories)
                ->withFoods($foods)
                ->withPackages($packages)
                ->withOrders($orders)
                ->withSetedCoupon($setedCoupon)
                ->withFoodsOffer($foodsOffer)
                ->withPackagesOffer($packagesOffer);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // Cashon or Digital Payment Confirmation
    public function confirmation(Request $request){

        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $orders = ($sessionOrder) ? json_decode($sessionOrder->orders, true):array();
        // dd($orders);

        if(!$orders) return redirect()->route('homepage');
        $setedType = json_decode( Cookie::get('setedType'), true);

        if( $setedType )  Cookie::queue(Cookie::forget('setedType'));


        $arr = [
            'delivery_type'     =>      $request->delivery_type,
            'payment_type'     =>      $request->payment_type,
        ];

        $setCookies = json_encode($arr);

        Cookie::queue(Cookie::make('setedType', $setCookies, time() + ( 86400 * 30 ) ));  #time() + ( 86400 * 30 );

        if ($request->payment_type == 1) return redirect()->route('customer.order.cash');
        return redirect()->route('customer.order.card');
    }

    public function getFoodName($id){
        $name = $this->food->find($id)->name;
        return response($name);
    }


    public function getFoodPrice($id){

        $offer = $this->offer->where('food_ids','like','%'.$id.'%')
                        ->where('exp_date', '>=', date('Y-m-d'))
                        ->orderBy('updated_at', 'desc')->first();

        $origin = $this->food->find($id)->price;

        if($offer){
            $price = $origin - ( ($origin * $offer->discount)/100 );
            return response($price);
        }
        return response($origin);
    }


    public function getPackageName($id){
        $name = $this->package->find($id)->name;
        return response($name);
    }
}
