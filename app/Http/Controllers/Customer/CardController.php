<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

Use App\Model\Backend\Food\Food;
Use App\Model\Backend\Offer\Offer;
Use App\Model\Backend\Package\Package;
Use App\Model\Backend\Order\Order;
use App\Model\Backend\UsedCoupon\UsedCoupon;
use Illuminate\Support\Facades\Notification;
use App\User;

use App\Notifications\UserOrdered;
use App\Model\Backend\SessionOrder\SessionOrder;

use Cookie;
use Auth;
use Carbon\Carbon;
use Stripe\Stripe;
use Stripe\Charge;

class CardController extends Controller
{
    private $food, $package, $order, $usedCoupon, $sessionOrder;
    private $session_id;

    public function __construct(){
        $this->food = new Food();
        $this->package = new Package();
        $this->order = new Order();
        $this->usedCoupon = new UsedCoupon();
        $this->sessionOrder = new SessionOrder();
        $this->session_id = null;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = null;
        $orders = ($sessionOrder) ? json_decode($sessionOrder->orders, true):array();
        if(!$orders) {
            return redirect()->route('homepage');
        } else {
            if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        }

        if( $setedCoupon )  $sessionOrder->setedCoupon = null;

        return view('frontend.order.card.index')
                ->withOrders($orders)
                ->withSetedCoupon($setedCoupon);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('frontend.order.card.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->session_id == null){
            $session_id = json_decode(Cookie::get('session_id'), true);
            $this->session_id = $session_id['id'];
        }

        // dd($request);
        $this->validate($request, [
            // 'order_type'          =>  'required | integer',
            'name'          =>  'required | string | max:100',
            'phone'         =>  'required | string | max:30',
            'country'       =>  'required | string',
            'city'          =>  'required | string',
            'area'          =>  'required | string',
            'address'       =>  'required | string',
        ]);

        if($request->order_type == 1){
            $this->validate($request, [
                'pre_time'      =>      'required | string',
            ]);
        }

        if($request->order_type == 2){
            $this->validate($request, [
                'post_time'      =>      'required | string',
                'post_date'      =>      'required | string',
            ]);
        }


        $sessionOrder = $this->sessionOrder->where('session_id',$this->session_id)->first();  #find if session data already save;
        $setedCoupon = array();
        $orders = array();

        if(!$sessionOrder) return redirect()->route('homepage');

        $orders = ($sessionOrder) ? json_decode($sessionOrder->orders, true):array();
        if( $sessionOrder->setedCoupon ) $setedCoupon = json_decode($sessionOrder->setedCoupon, true);
        $setedType = json_decode( Cookie::get('setedType'), true);

        $cashOrder = new Order();

        $cashOrder->user_id     =   Auth::user()->id;
        $cashOrder->country     =   $request->country;
        $cashOrder->city        =   $request->city;
        $cashOrder->area        =   $request->area;
        $cashOrder->phone       =   $request->phone;
        $cashOrder->address     =   $request->address;
        $cashOrder->delivery_type     =   $setedType["delivery_type"];
        $cashOrder->payment_type     =   $setedType["payment_type"];
        $cashOrder->order_type     =   $request->order_type;

        if($request->order_type == 1){
            $cashOrder->order_date     =   date('Y-m-d');
            $cashOrder->order_time     =   date('H:i:s', strtotime($request->pre_time));
        } else{
            $cashOrder->order_date     =   date('Y-m-d', strtotime($request->post_date));
            $cashOrder->order_time     =   date('H:i:s', strtotime($request->post_time));
        }

        $cashOrder->order_status     =   0;
        $cashOrder->order_list     =   json_encode( $orders );

        // dd($setedCoupon["coupon_id"]);

        if($setedCoupon){

            $usedCoupon = $this->usedCoupon->where(['user_id' => $setedCoupon["user_id"], 'coupon_id' => $setedCoupon["coupon_id"]])->first();

            if($usedCoupon){
                $usedCoupon->uses_limit = $setedCoupon["uses_limit"];
                $usedCoupon->save();
            } else {
                $usedCoupon = new UsedCoupon();
                $usedCoupon->coupon_id = $setedCoupon["coupon_id"];
                $usedCoupon->user_id = $setedCoupon["user_id"];
                $usedCoupon->uses_limit = $setedCoupon["uses_limit"];
                $usedCoupon->save();
            }
            $cashOrder->copon_id     =   $setedCoupon["coupon_id"];
            $cashOrder->original_price     =   $setedCoupon["total"];
            $cashOrder->total_discount  =    $setedCoupon["totalDiscount"];
            $cashOrder->reduced_price  =    $setedCoupon["reducedPrice"];

        } else {
            $item_id_originalPrice = array_column($orders, 'original_price');
            $item_id_price = array_column($orders, 'price');
            $original_price = 0;
            $price = 0;
            for($i =0; $i<sizeof($item_id_originalPrice); $i++){
                $original_price += $item_id_originalPrice[$i];
                $price += $item_id_price[$i];
            }

            $cashOrder->original_price = $original_price;
            $cashOrder->reduced_price = $price;
            $cashOrder->total_discount = $original_price - $price;

        }

        //================payment system==============
        Stripe::setApiKey("sk_test_nQFVvC3xWmrCIeEhJD9IO97e00hD0MDdid");

        $charge = Charge::create([
            'amount' => $cashOrder->reduced_price * 100,
            'currency' => 'usd',
            'source' => $request->stripeToken,
            'receipt_email' => 'jenny.rosen@example.com',
        ]);

        $cashOrder->payment_recept_url = $charge->receipt_url;
        //================payment system==============

        $cashOrder->save();
        Cookie::queue(Cookie::forget('setedType'));
        $sessionOrder->delete();

        $admins = User::where('role_id', 2)->get();
        // try {
        //     Notification::send($admins, new UserOrdered($cashOrder));
        // } catch(\Exception $e){

        // }
        foreach($admins as $admin){

            // dd($admin);
            $admin->notify(new UserOrdered($cashOrder));
        }

        return redirect()->route('customer.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
