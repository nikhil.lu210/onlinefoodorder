<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Model\Backend\DeliveryBoy\DeliveryBoy;
use App\Model\Backend\Order\Order;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = User::where('role_id', 3)->count();
        $admin = User::where('role_id', 2)->count();

        $deli_boy = DeliveryBoy::count();
        $tot_order = Order::count();
        $pen_order = Order::where('order_status', 0)->count();
        $pro_order = Order::where('order_status', 1)->count();
        $can_order = Order::where('order_status', -1)->count();
        
        return view('backend.super_admin.dashboard.index')
                ->withCustomer($customer)
                ->withAdmin($admin)
                ->withDeliBoy($deli_boy)
                ->withTotOrder($tot_order)
                ->withPenOrder($pen_order)
                ->withProOrder($pro_order)
                ->withCanOrder($can_order);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
