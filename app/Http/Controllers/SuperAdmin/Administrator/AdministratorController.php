<?php

namespace App\Http\Controllers\SuperAdmin\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Hash;

class AdministratorController extends Controller
{
    private $admin;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->admin = new User();
    }


    public function index()
    {
        $admins = $this->admin->all();
        return view('backend.super_admin.administrator.admin.index')->withAdmins($admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.super_admin.administrator.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'first_name'    => 'required | string | max:50',
            'last_name'     => 'required | string | max:50',
            'email'         => 'required | string | email | max:191 | unique:users',
            'username'      => 'required | string | max:20 | unique:users',
            'password'      => 'required | string | min:8',
            'terms'         => 'required',
        ]);

        $admin = new User();

        if ($request->terms == 'on') {
            $admin->username    =   $request->username;
            $admin->password    =   Hash::make($request->password);
            $admin->name        =   $request->first_name . " " .$request->last_name;
            $admin->email       =   $request->email;
            $admin->role_id     =   2;
            $admin->save();
        }

        return redirect()->route('superadmin.administrator.admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backend.super_admin.administrator.admin.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
