<?php

// Customer Routes
Route::group([
    'as' => 'cart.',
    'prefix' => 'cart',
    'namespace' => 'Customer'
],
    function(){
        Route::post('setFoodCookies/{id}', 'CartController@setFoodCookies')->name('setFoodCookies');
        Route::post('setPackageCookies/{id}', 'CartController@setPackageCookies')->name('setPackageCookies');
        Route::post('updateOrderCookies/{id}', 'CartController@updateOrderCookies')->name('updateOrderCookies');
        Route::get('destroyOrderCookies/{id}', 'CartController@destroyOrderCookies')->name('destroyCookies');
        Route::get('getFoodPackageName/{id}/{fo_pa}', 'CartController@getfoodPackageName');
    }
);
