<?php

// Customer Routes
Route::group([
    'as' => 'customer.',
    'prefix' => 'customer',
    'namespace' => 'Customer'
],
    function(){
        Route::get('/', 'HomeController@index')->name('home');
        Route::post('home/confirmation', 'HomeController@confirmation')->name('home.confirmation');
    }
);
