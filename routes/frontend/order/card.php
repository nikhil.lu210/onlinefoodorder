<?php

// Customer Routes
Route::group([
    'as' => 'customer.order.',
    'prefix' => 'customer/order',
    'namespace' => 'Customer'
],
    function(){
        Route::get('card', 'CardController@index')->name('card');
        Route::post('card/order/store', 'CardController@store')->name('card.store');
    }
);
