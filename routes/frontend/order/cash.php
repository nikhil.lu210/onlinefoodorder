<?php

// Customer Routes
Route::group([
    'as' => 'customer.order.',
    'prefix' => 'customer/order',
    'namespace' => 'Customer'
],
    function(){
        Route::get('cash', 'CashController@index')->name('cash');
        Route::post('cash/order/store', 'CashController@store')->name('cash.store');
    }
);
