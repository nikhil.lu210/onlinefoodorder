<?php

Route::group(
    [
        'as' => 'superadmin.record.',
        'prefix' => 'superadmin',
        'namespace' => 'SuperAdmin\Order'
    ],
    function () {
        Route::get('record/all_order', 'AllOrderController@index')->name('all.order');
        Route::get('record/all_order/{id}', 'AllOrderController@show')->name('all.order.show');
    }
);
