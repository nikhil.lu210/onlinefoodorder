<?php

Route::group(
    [
        'as' => 'superadmin.record.',
        'prefix' => 'superadmin',
        'namespace' => 'SuperAdmin\Order'
    ],
    function () {
        Route::get('record/completed_order', 'CompletedOrderController@index')->name('completed.order');
        Route::get('record/completed_order/{id}', 'CompletedOrderController@show')->name('completed.order.show');
    }
);
