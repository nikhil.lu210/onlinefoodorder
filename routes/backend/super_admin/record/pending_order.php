<?php

Route::group(
    [
        'as' => 'superadmin.record.',
        'prefix' => 'superadmin',
        'namespace' => 'SuperAdmin\Order'
    ],
    function () {
        Route::get('record/pending_order', 'PendingOrderController@index')->name('pending.order');
        Route::get('record/pending_order/{id}', 'PendingOrderController@show')->name('pending.order.show');
    }
);
