<?php

Route::group([
    'as' => 'superadmin.',
    'prefix' => 'superadmin',
    'namespace' => 'SuperAdmin'
],
    function(){
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    }
);
