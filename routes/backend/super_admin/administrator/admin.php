<?php

Route::group(
    [
        'as' => 'superadmin.administrator.',
        'prefix' => 'superadmin',
        'namespace' => 'SuperAdmin\Administrator'
    ],
    function () {
        Route::get('administrator/admin', 'AdministratorController@index')->name('admin');
        Route::get('administrator/admin/create', 'AdministratorController@create')->name('admin.create');
        Route::post('administrator/admin/store', 'AdministratorController@store')->name('admin.store');
        Route::get('administrator/admin/show/{id}', 'AdministratorController@show')->name('admin.show');
        Route::get('administrator/admin/destroy/{id}', 'AdministratorController@destroy')->name('admin.destroy');
    }
);
