<?php

Route::group(
    [
        'as' => 'admin.category.',
        'prefix' => 'admin',
        'namespace' => 'Admin\FoodMenu'
    ],
    function () {
        Route::get('food_menu/all_category', 'CategoryController@index')->name('all.category');
        Route::get('food_menu/add_new_category', 'CategoryController@create')->name('create.category');
        Route::post('food_menu/add_new_category/store', 'CategoryController@store')->name('store.category');
        Route::get('food_menu/destroy_category/{id}', 'CategoryController@destroy')->name('destroy.category');
        Route::get('food_menu/all_category/show/{id}', 'CategoryController@show')->name('show.category');
        Route::post('food_menu/all_category/update/{id}', 'CategoryController@update')->name('update.category');
    }
);
