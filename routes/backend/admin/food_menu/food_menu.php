<?php

Route::group(
    [
        'as' => 'admin.food.',
        'prefix' => 'admin',
        'namespace' => 'Admin\FoodMenu'
    ],
    function () {
        Route::get('food_menu/all_food_menu', 'FoodMenuController@index')->name('all.food_menu');
        Route::get('food_menu/add_new_food_menu', 'FoodMenuController@create')->name('create.food_menu');
        Route::post('food_menu/add_new_food_menu', 'FoodMenuController@store')->name('store.food_menu');
        Route::get('food_menu/food_item/destroy/{id}', 'FoodMenuController@destroy')->name('destroy.food_menu');
        Route::get('food_menu/all_food_menu/show/{id}', 'FoodMenuController@show')->name('show.food_menu');
        Route::post('food_menu/all_food_menu/update/{id}', 'FoodMenuController@update')->name('update.food_menu');
    }
);
