<?php

Route::group(
    [
        'as' => 'admin.food.',
        'prefix' => 'admin',
        'namespace' => 'Admin\FoodMenu'
    ],
    function () {
        Route::get('food_menu/all_food_package', 'FoodPackageController@index')->name('all.food_package');
        Route::get('food_menu/add_new_food_package', 'FoodPackageController@create')->name('create.food_package');
        Route::post('food_menu/add_new_food_package', 'FoodPackageController@store')->name('store.food_package');
        Route::get('food_menu/food_package/destroy/{id}', 'FoodPackageController@destroy')->name('destroy.food_package');
        Route::get('food_menu/all_food_package/show/{id}', 'FoodPackageController@show')->name('show.food_package');
        Route::post('food_menu/all_food_package/update/{id}', 'FoodPackageController@update')->name('update.food_package');
    }
);
