<?php

Route::group([
    'as' => 'admin.',
    'prefix' => 'admin',
    'namespace' => 'Admin'
],
    function(){
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    }
);
