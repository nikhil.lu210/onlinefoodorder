<?php

Route::group(
    [
        'as' => 'admin.record.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Order'
    ],
    function () {
        Route::get('record/processing_order', 'ProcessingOrderController@index')->name('processing.order');
        Route::get('record/processing_order/show/{id}', 'ProcessingOrderController@show')->name('processing.order.show');
    }
);
