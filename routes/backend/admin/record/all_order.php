<?php

Route::group(
    [
        'as' => 'admin.record.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Order'
    ],
    function () {
        Route::get('record/all_order', 'AllOrderController@index')->name('all.order');
        Route::get('record/all_order/show/{id}', 'AllOrderController@show')->name('all.order.show');
        Route::post('record/all_order/confirm/{id}', 'AllOrderController@confirm')->name('order.confirm');
        Route::post('record/all_order/cancel/{id}', 'AllOrderController@cancel')->name('order.cancel');
        Route::get('record/all_order/complete/{id}', 'AllOrderController@complete')->name('order.complete');
    }
);
