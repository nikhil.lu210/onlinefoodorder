<?php

Route::group(
    [
        'as' => 'admin.record.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Order'
    ],
    function () {
        Route::get('record/completed_order', 'CompletedOrderController@index')->name('completed.order');
        Route::get('record/completed_order/show/{id}', 'CompletedOrderController@show')->name('completed.order.show');
    }
);
