<?php

Route::group(
    [
        'as' => 'admin.record.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Order'
    ],
    function () {
        Route::get('record/canceled_order', 'CanceledOrderController@index')->name('canceled.order');
        Route::get('record/canceled_order/show/{id}', 'CanceledOrderController@show')->name('canceled.order.show');
    }
);
