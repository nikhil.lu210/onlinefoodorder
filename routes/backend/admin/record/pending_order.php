<?php

Route::group(
    [
        'as' => 'admin.record.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Order'
    ],
    function () {
        Route::get('record/pending_order', 'PendingOrderController@index')->name('pending.order');
        Route::get('record/pending_order/show/{id}', 'PendingOrderController@show')->name('pending.order.show');
    }
);
