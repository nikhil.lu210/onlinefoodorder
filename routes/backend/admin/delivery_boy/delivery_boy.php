<?php

Route::group(
    [
        'as' => 'admin.delivery.',
        'prefix' => 'admin',
        'namespace' => 'Admin\DeliveryBoy'
    ],
    function () {
        Route::get('delivery_boy/all_delivery_boy', 'DeliveryBoyController@index')->name('all.delivery_boy');
        Route::get('delivery_boy/add_new_delivery_boy', 'DeliveryBoyController@create')->name('create.delivery_boy');
        Route::post('delivery_boy/add_new_delivery_boy', 'DeliveryBoyController@store')->name('store.delivery_boy');
        Route::get('delivery_boy/destroy/{id}','DeliveryBoyController@destroy')->name('destroy.delivery_boy');
        Route::get('delivery_boy/all_delivery_boy/show/{id}','DeliveryBoyController@show')->name('show.delivery_boy');
        Route::post('delivery_boy/all_delivery_boy/update/{id}','DeliveryBoyController@update')->name('update.delivery_boy');
    }
);
