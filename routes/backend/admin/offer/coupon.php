<?php

Route::group(
    [
        'as' => 'admin.offer.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Offer'
    ],
    function () {
        Route::get('offer/all_coupon', 'CouponController@index')->name('all.coupon');
        Route::get('offer/add_new_coupon', 'CouponController@create')->name('create.coupon');
        Route::post('offer/add_new_coupon', 'CouponController@store')->name('store.coupon');
        Route::get('offer/destroy_coupon/{id}', 'CouponController@destroy')->name('destroy.coupon');
        Route::get('offer/all_coupon/show/{id}', 'CouponController@show')->name('show.coupon');
        Route::post('offer/all_coupon/update/{id}', 'CouponController@update')->name('update.coupon');
    }
);
