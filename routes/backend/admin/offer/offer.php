<?php

Route::group(
    [
        'as' => 'admin.offer.',
        'prefix' => 'admin',
        'namespace' => 'Admin\Offer'
    ],
    function () {
        Route::get('offer/all_offer', 'OfferController@index')->name('all.offer');
        Route::get('offer/add_new_offer', 'OfferController@create')->name('create.offer');
        Route::post('offer/add_new_offer', 'OfferController@store')->name('store.offer');
        Route::get('offer/destroy_offer/{id}', 'OfferController@destroy')->name('destroy.offer');
        Route::get('offer/all_offer/show/{id}', 'OfferController@show')->name('show.offer');
        Route::post('offer/all_offer/update/{id}', 'OfferController@update')->name('update.offer');
    }
);
