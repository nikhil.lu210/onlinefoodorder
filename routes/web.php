<?php

// Route::get('/', function () {
//     return redirect()->route('customer.home');
// });

use App\User;

Auth::routes();

Route::post('/loginwithuser', 'Auth\LoginController@loginWithEmailOrUsername')->name('loginWithUser');



/*===================================
========< Super Admin Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'superadmin']
    ],
    function () {

        // Dashboard
        include_once 'backend/super_admin/dashboard.php';

        // Records
        include_once 'backend/super_admin/record/all_order.php';
        include_once 'backend/super_admin/record/completed_order.php';
        include_once 'backend/super_admin/record/pending_order.php';

        // Administrator
        include_once 'backend/super_admin/administrator/admin.php';
    }
);



/*===================================
===========< Admin Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'admin']
    ],
    function () {

        // Dashboard
        include_once 'backend/admin/dashboard.php';

        // Records
        include_once 'backend/admin/record/all_order.php';
        include_once 'backend/admin/record/completed_order.php';
        include_once 'backend/admin/record/pending_order.php';
        include_once 'backend/admin/record/processing_order.php';
        include_once 'backend/admin/record/canceled_order.php';

        // Food Menus
        include_once 'backend/admin/food_menu/food_package.php';
        include_once 'backend/admin/food_menu/food_menu.php';
        include_once 'backend/admin/food_menu/category.php';

        // Delivery Boy
        include_once 'backend/admin/delivery_boy/delivery_boy.php';

        // Offers
        include_once 'backend/admin/offer/coupon.php';
        include_once 'backend/admin/offer/offer.php';
    }
);



/*===================================
==========< Customer Routes >========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'customer']
    ],
    function () {

        include_once 'frontend/customer/home.php';
        include_once 'frontend/order/cash.php';
        include_once 'frontend/order/card.php';
    }
);

//for destroy all old order
Route::get('destroyoldorder', 'Customer\CashController@destroyoldorder');


/*===================================
==========< Guest Routes >========
===================================*/
Route::group(
    [
        // 'middleware' => ['guest', 'auth', 'customer']
    ],
    function () {
        Route::get('/', 'Customer\HomeController@index')->name('homepage');
        Route::post('cart/calculateCoupon', 'Customer\CartController@calculateCoupon')->name('cart.calculateCoupon');

        include_once 'frontend/customer/cart_cookies.php';
    }
);

Route::get('/mark-all-read/{user}/{id}', function (User $user, $id) {
    $user->unreadNotifications->where('id', $id)->markAsRead();
    return response(['message'=>'done', 'notifications'=>$user->unreadNotifications]);
});




Route::get('/customer/home/getFoodName/{id}', 'Customer\HomeController@getFoodName');
Route::get('/customer/home/getFoodPrice/{id}', 'Customer\HomeController@getFoodPrice');
Route::get('/customer/home/getPackagesName/{id}', 'Customer\HomeController@getPackageName');
