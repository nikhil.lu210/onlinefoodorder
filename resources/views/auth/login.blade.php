<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    {{-- Required meta tags --}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{-- CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--  Page Title  --}}
    <title> Restaurent Name | Login </title>
    <link rel="shortcut icon" href="{{ asset('frontend/images/icon.png') }}" type="image/x-icon">

    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

    {{-- Fontawesome CSS --}}
    <link rel="stylesheet" href="{{ asset('frontend/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/fontawesome.min.css') }}">

    {{-- UI-Kit CSS --}}
    <link rel="stylesheet" href="{{ asset('frontend/uikit/css/assets/input.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/uikit/css/global-style.css') }}">

    {{-- Custom CSS --}}
    <style>
        /* body{
            background: #fefefe;
        } */
        .login-part .content{
            border-radius: 10px;
            box-shadow: 0px 0px 30px -7px rgba(0, 0, 0, 0.2);
        }
        .login-part .content .heading{
            padding: 20px;
            background-color: #fec62b;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }
        .login-part .content .heading h2{
            color: #fff;
            font-weight: 700;
        }
        .login-part .content .forms{
            padding: 60px;
        }
        .login-part .content .forms .input-field{
            padding-bottom: 25px;
        }
        .login-part .content .form-control {
            height: calc(1.5em + 1rem + 10px);
            padding: .375rem 1.2rem;
            font-weight: 500;
            color: #495057;
            background-color: #f5f5f5;
            border: 1px solid #ffffff;
            border-radius: 30px;
        }
        .login-part .content .form-control:focus,
        .login-part .content .form-control:active,
        .login-part .content .form-control.active{
            outline: none;
            box-shadow: 0px 0px 30px -7px rgba(0, 0, 0, 0.1);
            background-color: #fff;
            transition: 0.3s all ease-in-out;
        }


        .login-part .content .submit-button{
            padding-bottom: 10px;
        }
        .login-part .content .btn-submit {
            height: calc(1.5em + 1rem + 10px);
            padding: .375rem 1.2rem;
            font-weight: 700;
            color: #fff;
            background-color: #fec62b;
            border: 1px solid #fec62b;
            border-radius: 30px;
        }
        .login-part .content .btn-submit:hover,
        .login-part .content .btn-submit:active,
        .login-part .content .btn-submit:focus {
            background-color: #fec62b;
            border: 1px solid #fec62b;
            outline: none !important;
            box-shadow: 0px 0px 30px -7px rgba(0, 0, 0, 0.2);
        }

        .frgt-pass a{
            color: #333;
            padding-left: 5px;
            font-size: 14px;
            font-weight: 600;
        }
        .new-reg a{
            color: #333;
            padding-right: 5px;
            font-size: 14px;
            font-weight: 600;
        }
        a:hover{
            color: #fec62b;
        }
    </style>
</head>

<body>

    <section class="login-part">
        <div class="overlay">
            <div class="container h-100">
                <div class="row h-100 justify-content-center align-items-center">
                    <div class="col-md-6">
                        <div class="content text-center">
                            <div class="heading text-center">
                                <h2>Sign In</h2>
                            </div>
                            <form action="{{ route('loginWithUser') }}" method="post">
                            @csrf
                                <div class="row forms">
                                    <div class="col-12 input-field">
                                        <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email / Username *" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-12 input-field">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password *" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-12 submit-button">
                                        <button class="btn btn-outline-dark btn-submit btn-block">Sign In</button>
                                    </div>

                                    <div class="col-12">
                                        <div class="frgt-pass">
                                            @if (Route::has('password.request'))
                                                <a class="float-left" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                        <div class="new-reg">
                                            <a class="float-right" href="{{ route('register') }}">Don't Have Account?</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('frontend/js/jquery-3.3.1.slim.min.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>

    <!-- Custom JS -->
    <script src="{{ asset('frontend/uikit/js/main.js') }}"></script>

    <!-- Custom JS -->
</body>

</html>
