<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                <b>Super Admin Dashboard</b>
            </div>
            <div class="sidebar-toggle lg hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-left" aria-label="Toggle sidebar"></i>
            </div>
            <div class="sidebar-toggle sm hidden-xs d-none" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-right" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">
                    <ul class="nav nav-main">

                        {{-- DashBoard --}}
                        <li class="{{ Request::is('superadmin/dashboard*') ? 'nav-active' : '' }}">
                            <a href="{{ Route('superadmin.dashboard') }}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        {{-- Records --}}
                        <li class="nav-parent {{ Request::is('superadmin/record/*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-cloud" aria-hidden="true"></i>
                                <span>Records</span>
                            </a>
                            {{-- {{ dd(Request::path()) }} --}}
                            <ul class="nav nav-children">
                                <li class="{{ Request::is('superadmin/record/all_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.record.all.order') }}">All Orders</a>
                                </li>
                                <li class="{{ Request::is('superadmin/record/pending_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.record.pending.order') }}">Pending Orders</a>
                                </li>
                                <li class="{{ Request::is('superadmin/record/completed_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.record.completed.order') }}">Completed Orders</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Administrator --}}
                        <li class="nav-parent {{ Request::is('superadmin/administrator/*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-gamepad" aria-hidden="true"></i>
                                <span>Administrator</span>
                            </a>
                            <ul class="nav nav-children">
                                <li class="{{ Request::is('superadmin/administrator/admin') ? 'nav-active' : Request::is('superadmin/administrator/admin/show*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.administrator.admin') }}">All Admin</a>
                                </li>
                                <li class="{{ Request::is('superadmin/administrator/admin/create*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.administrator.admin.create') }}">Assign New Admin</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>

    </aside>
    <!-- end: sidebar -->
