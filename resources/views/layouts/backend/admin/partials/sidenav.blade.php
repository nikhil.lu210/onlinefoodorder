<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                <b>Admin Dashboard</b>
            </div>
            <div class="sidebar-toggle lg hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-left" aria-label="Toggle sidebar"></i>
            </div>
            <div class="sidebar-toggle sm hidden-xs d-none" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-right" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">
                    <ul class="nav nav-main">

                        {{-- Dashboard --}}
                        <li class="{{ Request::is('admin/dashboard*') ? 'nav-active' : '' }}">
                            <a href="{{ Route('admin.dashboard') }}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        {{-- Records --}}
                        <li class="nav-parent {{ Request::is('admin/record/*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-cloud" aria-hidden="true"></i>
                                <span>Records</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('admin/record/all_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.record.all.order') }}">All Orders</a>
                                </li>
                                <li class="{{ Request::is('admin/record/pending_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.record.pending.order') }}">Pending Orders</a>
                                </li>
                                <li class="{{ Request::is('admin/record/processing_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.record.processing.order') }}">Processing Orders</a>
                                </li>
                                <li class="{{ Request::is('admin/record/completed_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.record.completed.order') }}">Completed Orders</a>
                                </li>

                                <li class="{{ Request::is('admin/record/canceled_order*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.record.canceled.order') }}">Canceled Orders</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Foods --}}
                        <li class="nav-parent {{ Request::is('admin/food_menu*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-coffee" aria-hidden="true"></i>
                                <span>Food Menus</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('admin/food_menu/all_food_package*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.food.all.food_package') }}">All Food Package</a>
                                </li>
                                <li class="{{ Request::is('admin/food_menu/add_new_food_package*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.food.create.food_package') }}">Add Package</a>
                                </li>

                                <li class="{{ Request::is('admin/food_menu/all_food_menu*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.food.all.food_menu') }}">All Food Menus</a>
                                </li>
                                <li class="{{ Request::is('admin/food_menu/add_new_food_menu*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.food.create.food_menu') }}">Add Food</a>
                                </li>

                                <li class="{{ Request::is('admin/food_menu/all_category*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.category.all.category') }}">All Category</a>
                                </li>
                                <li class="{{ Request::is('admin/food_menu/add_new_category*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.category.create.category') }}">Add Category</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Delivery Boys --}}
                        <li class="nav-parent {{ Request::is('admin/delivery_boy*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Delivery Boys</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('admin/delivery_boy/all_delivery_boy*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.delivery.all.delivery_boy') }}">All Delivery Boys</a>
                                </li>
                                <li class="{{ Request::is('admin/delivery_boy/add_new_delivery_boy*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.delivery.create.delivery_boy') }}">Add Delivery Boy</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Offers --}}
                        <li class="nav-parent {{ Request::is('admin/offer*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-slack" aria-hidden="true"></i>
                                <span>Offers</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('admin/offer/all_coupon*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.offer.all.coupon') }}">All Coupons</a>
                                </li>
                                <li class="{{ Request::is('admin/offer/add_new_coupon*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.offer.create.coupon') }}">Add Coupon</a>
                                </li>
                                <li class="{{ Request::is('admin/offer/all_offer*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.offer.all.offer') }}">All Offers</a>
                                </li>
                                <li class="{{ Request::is('admin/offer/add_new_offer*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('admin.offer.create.offer') }}">Add New Offer</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>

    </aside>
    <!-- end: sidebar -->
