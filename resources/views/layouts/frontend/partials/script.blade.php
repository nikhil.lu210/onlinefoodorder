<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
<script src="{{ asset('frontend/js/jquery-3.3.1.slim.min.js') }}"></script>
<script src="{{ asset('frontend/js/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>

<!-- Custom JS -->
<script src="{{ asset('frontend/uikit/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('frontend/uikit/js/main.js') }}"></script>

<!-- Date-Time Picker -->
<script src="{{ asset('frontend/js/bootstrapDatepickr-1.0.0.min.js') }}"></script>

{{-- Multiple Select --}}
<script src="{{ asset('frontend/js/bootstrap-select.min.js') }}"></script>

{{-- Vue Js --}}
{{-- <script src="{{ asset('frontend/js/vue/vue.min.js') }}"></script> --}}

<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
        return false;
    }

    // DatePicker
    $("#datePick").bootstrapDatepickr({
        date_format: "d-m-Y"
    });
</script>

<!-- Custom JS -->
<script src="{{ asset('frontend/js/script.js') }}"></script>
<script src="{{ asset('frontend/js/responsive.js') }}"></script>

@yield('scripts')
