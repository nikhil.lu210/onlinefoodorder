<!-- ===================================================
        ========= Small Device Cart Part Starts ===========
==================================================== -->
<div class="cart-btn-div">
    <a href="#" class="btn btn-outline-dark btn-lg btn-sm-cart" data-toggle="modal" data-target="#smallDeviceCart">
        <i class="fab fa-opencart"><sup class="order-amount" id="total_orders_small">{{ sizeof($orders) }}</sup></i>
    </a>
</div>
<!-- =================================================
            ========= Small Device Cart Part Ends ===========
        ================================================== -->

<!-- ================================
            ========= Small Device Cart Starts ===========
    ================================= -->
<div class="modal fade custom-modal" id="smallDeviceCart" data-modal-index="1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title float-left" id="smallDeviceCartLabel">Your Total Order</h6>
                <div class="float-right d-flex">
                    <a href="#" class="btn btn-outline-dark btn-sm btn-modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="small-device-cart-part">
                            <div class="cart-info">
                                <div class="items">
                                     <table class="table table-borderless">
                                          <tbody class="cart-items" id="cart_items_small">
                                                <?php $total = 0; $show_total=0; ?>
                                                @foreach ($orders as $key => $order)
                                                    <tr class="h-100">
                                                            <td class="total-item-td h-100 justify-content-center align-items-center">
                                                                <div class="total-item">{{ $order["quantity"] }}</div>
                                                            </td>
                                                            <td class="food-name-td">
                                                                <?php
                                                                     $name = ($order["food_package"] == "food") ? App\Model\Backend\Food\Food::find($order["id"])->name:
                                                                     App\Model\Backend\Package\Package::find($order["id"])->name;
                                                                     $total += $order["original_price"];
                                                                     $show_total += $order["price"]
                                                                ?>
                                                                <a href="#" class="food-name"  data-toggle="modal" data-target="#order_{{ $order["id"].$order["food_package"] }}">{{ $name }}</a>
                                                            </td>
                                                            <td class="cart-price-td">
                                                                <div class="cart-price">
                                                                    <h6>$@if($setedCoupon){{ $order["original_price"] }}@else{{ $order["price"] }}@endif</h6>
                                                                </div>
                                                            </td>
                                                            <td class="remove-cart-td">
                                                                <div class="remove-cart-btn">
                                                                    <button id="remove_cart_{{ $key }}" class="btn btn-outline-dark btn-sm btn-remove-cart" onclick="return ConfirmDelete();"><i class="fas fa-minus"></i></button>
                                                                </div>
                                                            </td>
                                                    </tr>
                                               @endforeach
                                          </tbody>
                                          <tbody>
                                            <form id="set-coupon-small">
                                                <tr class="coupon-tr">
                                                        <td colspan="3" class="coupon-input-td"><input class="form-control form-control-sm coupon-input" id="set-coupon-input-small" type="text" placeholder="Coupon Code" name="coupon_code" required></td>
                                                        <td class="coupon-btn-td"><button class="btn btn-outline-dark btn-sm btn-block btn-coupon btn-coupon-small"><i class="fas fa-angle-right"></i></button></td>
                                                </tr>
                                            </form>

                                        </tbody>
                                     </table>

                                     <table class="table table-borderless">
                                          <tbody>
                                            <tr>
                                                 <td colspan="2"><strong>Total Price: </strong></td>
                                                 <td class="text-right" id="total_price_small"><strong>$@if($setedCoupon){{ number_format($total, 2) }}@else{{ number_format($show_total, 2) }}@endif</strong></td>
                                            </tr>
                                            <tr>
                                                 <td colspan="2"><strong>Total Discount: </strong></td>
                                                 <td class="text-right" id="total_discount_small"><strong>$@if($setedCoupon){{ number_format($setedCoupon["totalDiscount"], 2) }}@else{{ 0.00 }}@endif</strong></td>
                                            </tr>
                                            <tr id="app_id">
                                                 <td colspan="2"><strong>Discount Price: </strong></td>
                                                 <td class="text-right" id="after_discount_small"><strong>$@if($setedCoupon){{ number_format($setedCoupon["reducedPrice"], 2) }}@else{{ number_format($show_total, 2) }}@endif</strong></td>

                                            </tr>
                                       </tbody>
                                     </table>

                                     <form action="{{ route('customer.home.confirmation') }}" method="post">
                                    @csrf
                                     <div class="row food-receive">
                                          <div class="col-md-6 text-left">
                                               <div class="form-check">
                                                    <input class="form-check-input pickup-input" type="radio" value="1" id="pickup" name="delivery_type">
                                                    <label class="form-check-label pickup" for="pickup">
                                                        Pickup
                                                    </label>
                                               </div>
                                          </div>
                                          <div class="col-md-6 text-left">
                                               <div class="form-check">
                                                    <input class="form-check-input delivery-input" type="radio" value="2" id="delivery" name="delivery_type">
                                                    <label class="form-check-label delivery" for="delivery">
                                                        Delivery
                                                    </label>
                                               </div>
                                          </div>
                                     </div>

                                     <div class="row food-payment">
                                          <div class="col-md-6 text-left">
                                               <div class="form-check">
                                                    <input class="form-check-input cashon-input" type="radio" value="1" id="cashon" name="payment_type">
                                                    <label class="form-check-label cashon" for="cashon">
                                                        Cashon
                                                    </label>
                                               </div>
                                          </div>
                                          <div class="col-md-6 text-left">
                                               <div class="form-check">
                                                    <input class="form-check-input digital-input" type="radio" value="2" id="digital" name="payment_type">
                                                    <label class="form-check-label digital" for="digital">
                                                        VCard
                                                    </label>
                                               </div>
                                          </div>
                                     </div>

                                     <div class="row h-100 justify-content-center align-items-center confirm-btn">
                                          <div class="col-md-10">
                                                <button class="btn btn-dark btn-confirm btn-block btn-display btn-lg" disabled>Confirm Order</button>

                                                <button type="submit" class="btn btn-dark btn-confirm btn-cashon-vcard btn-block btn-lg d-none">Confirm Order</button>
                                          </div>
                                     </div>
                                    </form>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ================================
            ========= Small Device Cart Ends ===========
        ================================= -->

<!-- ================================
            ========= Cart Modal Starts ===========
    ================================= -->
@foreach ($orders as $list => $order)
    <div class="modal fade custom-modal custom-modal-sm-device-cart" id="order_{{ $order["id"].$order["food_package"] }}" data-modal-index="2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- {{ dd($orders) }} --}}
                    <?php
                        $name = ($order["food_package"] == "food") ? App\Model\Backend\Food\Food::find($order["id"])->name:
                        App\Model\Backend\Package\Package::find($order["id"])->name;
                        $total += $order["price"];
                   ?>
                    <h6 class="modal-title float-left" id="cartItemIdLabel">{{ $name }}</h6>
                    <div class="float-right d-flex">
                        <h6 class="modal-title  text-muted" id="cartItemIdPrice">Total Price: {{ $order["price"] }}</h6>
                        <a href="#" class="btn btn-outline-dark btn-sm btn-modal-close" data-dismiss="modal" aria-label="Close">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="sl-no">@if($list+1 < 10){{ 0 }}@endif{{ $list+1  }}</div>
                        </div>

                        <div class="col-md-9">
                            <div class="order-food-details">
                                <p class="food-details"> {{ $order["note"] }} </p>

                                <form id="cart_form_{{ $list }}">
                                    <input type="number" value="{{ $list }}" class="d-none">
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="total-qn text-muted text-right" style="padding-top: 5px;">Total Quantity: </h5>
                                        </div>
                                        <div class="col">
                                            <div class="quantity">
                                                <input type="number" name="quantity" class="form-control quantity-input" min="1" max="100" step="1" value="{{ $order["quantity"] }}">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="extra-items">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr class="extra-item-header">
                                                            <th colspan="4">Extra Items </th>
                                                        </tr>
                                                        @if($order["ex_quantity"])
                                                            @php
                                                                $arr = (array) $order["ex_quantity"];
                                                                // dd($arr);
                                                            @endphp
                                                            {{-- @while (list($key, $ex_order) = each($arr)) --}}
                                                            @foreach($arr as $key => $ex_order)
                                                                <tr class="table-row" id="row_{{ $key }}">
                                                                    <td class="select-or-not">
                                                                    <label class="custom-control custom-checkbox">
                                                                        <input name="status" type="checkbox" class="custom-control-input status" id="status_{{ $key }}" checked>
                                                                        <span class="custom-control-indicator"></span>
                                                                    </label>
                                                                    </td>
                                                                    <td class="modal-food-name-td">
                                                                        <h6>{{ App\Model\Backend\Food\Food::find($key)->name }}</h6>
                                                                    </td>
                                                                    <td class="modal-cart-price-td">
                                                                        <?php
                                                                            $offer = App\Model\Backend\Offer\Offer::where('food_ids','like','%'.$key.'%')->where('exp_date', '>=', date('Y-m-d'))->orderBy('updated_at', 'desc')->first();
                                                                            if($offer)
                                                                            $price  =   number_format(App\Model\Backend\Food\Food::find($key)->price - ( (App\Model\Backend\Food\Food::find($key)->price * $offer->discount)/100 ), 2);
                                                                    ?>
                                                                    <h6>$@if($offer){{ $price }}@else{{ App\Model\Backend\Food\Food::find($key)->price }}@endif</h6>
                                                                    </td>
                                                                    <td class="modal-quantity-cart-td" id="row_id_{{ $key }}">
                                                                        <div class="quantity">
                                                                            <input type="number" name="ex_quantity[{{ $key }}]" class="form-control quantity-input extra-item-quantity" min="1" max="100" step="1" value="{{ $ex_order }}" id="extra_item_id_{{ $key }}">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            {{-- @endwhile --}}
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="notes-input">
                                            <textarea class="form-control note-input" name="note" id="notePick" rows="3" placeholder="Your Extra Requirements">{{ $order["note"] }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="order-confirmation float-right">
                                                <button type="submit" class="btn btn-outline-dark btn-lg btn-add-to-cart btn-update-cart">
                                                    Update Cart
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
<!-- ================================
        ========= Cart Modal Ends ===========
    ================================= -->
