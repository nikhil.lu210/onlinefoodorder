<!-- ================================
========= Navbar Starts ===========
================================= -->
<header class="bg_f8f fire_header4 header_common">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
            <a class="navbar-brand order-1 order-lg-1" href="#"><img src="{{ asset('frontend/images/icon.png') }}" alt="logo"></a>
            <!-- <button class="navbar-toggler order-3" type="button" data-toggle="collapse" data-target="#navbar4" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button> -->
            <button class="navbar-toggler order-3" type="button" data-toggle="collapse" data-target="#navbar4" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>

            <div class="collapse navbar-collapse order-4 order-lg-2" id="navbar4">
                <ul class="navbar-nav m-auto">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Online Order</a></li>
                    <li><a href="#">Contact</a></li>

                    {{-- Small Device Menu/Profile/Login/Logout --}}
                    @guest
                        <li class="d-lg-none"><a href="{{ route('login') }}">Login</a></li>
                        @else
                        <li class="dropdown d-lg-none">
                            <a href="javascript:void(0)">
                                @if(isset(Auth::user()->avatar))
                                    <img src="{{ asset('user_files/'.Auth::user()->avatar) }}" alt="Profile Image" class="img-responsive" style="max-width: 27px; border-radius: 40px;">
                                @else
                                    <img src="{{ asset('frontend/images/profile.png') }}" alt="Profile Image" class="img-responsive" style="max-width: 27px; border-radius: 40px;">
                                @endif 
                                {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="custom_dropdown">
                                <li><a class="dropdown-item" href="#">Profile</a></li>
                                <li><a class="dropdown-item" href="#">Settings</a></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                    
                </ul>
            </div>

            {{-- Large Device Menu/Profile/Login/Logout --}}
            <ul class="navbar-nav pos_relative order-2 order-lg-3 d-lg-block d-sm-none device-hide">

                @guest
                    <li class="dropdown signin-signup">
                        <a href="javascript:void(0)">
                            Signin / Signup <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="custom_dropdown">
                            <li><a class="dropdown-item" href="{{ route('login') }}">Login</a></li>
                            <li><a class="dropdown-item" href="{{ route('register') }}">Register</a></li>
                        </ul>
                    </li>

                    @else
                        <li class="dropdown">
                            <a href="javascript:void(0)">
                                @if(isset(Auth::user()->avatar))
                                    <img src="{{ asset('user_files/'.Auth::user()->avatar) }}" alt="Profile Image" class="img-responsive" style="max-width: 27px; border-radius: 40px;">
                                @else
                                    <img src="{{ asset('frontend/images/profile.png') }}" alt="Profile Image" class="img-responsive" style="max-width: 27px; border-radius: 40px;">
                                @endif 
                                {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="custom_dropdown">
                                <li><a class="dropdown-item" href="#">Profile</a></li>
                                <li><a class="dropdown-item" href="#">Settings</a></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                @endguest
            </ul>
        </div>
    </nav>
</header>

<!-- ================================
========= Navbar Ends ===========
================================= -->
