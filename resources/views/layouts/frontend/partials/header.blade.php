{{-- Required meta tags --}}
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{{-- CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>

{{--  Page Title  --}}
<title> Restaurent Name @yield('page_title') </title>
<link rel="shortcut icon" href="{{ asset('frontend/images/icon.png') }}" type="image/x-icon">

{{-- Bootstrap CSS --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

{{-- Fontawesome CSS --}}
<link rel="stylesheet" href="{{ asset('frontend/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/fontawesome.min.css') }}">

{{-- UI-Kit CSS --}}
<link rel="stylesheet" href="{{ asset('frontend/uikit/css/assets/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/uikit/css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/uikit/css/global-style.css') }}">

{{-- Date-Time Picker --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrapDatepickr-1.0.0.min.css') }}">

{{-- Multiple Select --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap-select.css') }}" />

{{-- Custom CSS --}}
{{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
