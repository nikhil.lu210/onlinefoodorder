<!-- ================================
         ========= Menu Modal Starts ===========
    ================================= -->
    @foreach ($foods as $key => $food)

    <div class="modal fade custom-modal" id="food_{{ $food->id.$food->cat_id }}" tabindex="-1" role="dialog" aria-labelledby="modalIdLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
              <div class="modal-content">
                   <div class="modal-header">
                        <h6 class="modal-title float-left" id="modalIdLabel">{{ $food->name }}</h6>
                        <div class="float-right d-flex">
                             <h6 class="modal-title  text-muted" id="modalIdPrice">Total Price: $@if($foodsOffer[$key]["price"]){{ $foodsOffer[$key]["price"] }} @else{{ $food->price }} @endif</h6>
                             <a href="#" class="btn btn-outline-dark btn-sm btn-modal-close" data-dismiss="modal" aria-label="Close">
                                  <i class="fas fa-times"></i>
                             </a>
                        </div>
                   </div>
                   <div class="modal-body">
                        <div class="row">
                             <div class="col-md-3">
                                  <div class="sl-no">
                                       @if($key+1 < 10){{ 0 }}@endif{{ $key+1  }}
                                  </div>
                             </div>

                             <div class="col-md-9">
                                  <div class="order-food-details">
                                       <p class="food-details">
                                            {{ $food->note }}
                                       </p>
                                       <form id="food_form_{{ $food->id.$food->cat_id }}">
                                            {{-- action="{{ route('cart.setFoodCookies', ['id' => $food->id]) }}" --}}
                                            {{-- @csrf --}}
                                            <input type="number" value="{{ $food->id }}" class="d-none">
                                            <div class="row">
                                                 <div class="col">
                                                 <h5 class="total-qn text-muted text-right" style="padding-top: 5px;">Total Quantity: </h5>
                                                 </div>
                                                 <div class="col">
                                                      <div class="quantity">
                                                           <input type="number" name="quantity" class="form-control quantity-input" min="1" max="100" step="1" value="1">
                                                      </div>
                                                 </div>

                                                 @isset($food->extra_food_list[0])
                                                 <div class="col-md-12">
                                                      <div class="extra-items">
                                                           <table class="table table-borderless">
                                                                <tbody>
                                                                     <tr class="extra-item-header">
                                                                          <th colspan="4">Extra Items</th>
                                                                     </tr>
                                                                     @foreach ($food->extra_food_list as $ex_food)
                                                                     <tr class="table-row" id="row_{{ $ex_food }}">
                                                                          <td class="select-or-not">
                                                                          <label class="custom-control custom-checkbox">
                                                                               <input name="status" type="checkbox" class="custom-control-input status" id="status_{{ $ex_food }}">
                                                                               <span class="custom-control-indicator"></span>
                                                                          </label>
                                                                          </td>
                                                                          <td class="modal-food-name-td">
                                                                               <h6>{{ App\Model\Backend\Food\Food::find($ex_food)->name }}</h6>
                                                                          </td>
                                                                          <td class="modal-cart-price-td">
                                                                               <?php
                                                                                    $offer = App\Model\Backend\Offer\Offer::where('food_ids','like','%'.$ex_food.'%')->where('exp_date', '>=', date('Y-m-d'))->orderBy('updated_at', 'desc')->first();
                                                                                    if($offer)
                                                                                    $price  =   App\Model\Backend\Food\Food::find($ex_food)->price - ( (App\Model\Backend\Food\Food::find($ex_food)->price * $offer->discount)/100 );
                                                                               ?>
                                                                               <h6>$@if($offer){{ $price }}@else{{ App\Model\Backend\Food\Food::find($ex_food)->price }}@endif</h6>
                                                                          </td>
                                                                          <td class="modal-quantity-cart-td" id="row_id_{{ $ex_food }}">
                                                                               <div class="quantity">
                                                                                    <input type="number" name="ex_quantity[{{ $ex_food }}]" class="form-control quantity-input extra-item-quantity" min="1" max="100" step="1" value="1" disabled id="extra_item_id_{{ $ex_food }}">
                                                                               </div>
                                                                          </td>
                                                                     </tr>
                                                                     @endforeach
                                                                </tbody>
                                                           </table>
                                                      </div>
                                                      </div>
                                                 @endisset



                                                 <div class="col-md-12">
                                                      <div class="notes-input">
                                                           <textarea class="form-control note-input" name="note" id="notePick" rows="3" placeholder="Your Extra Requirements"></textarea>
                                                      </div>
                                                 </div>
                                                 <div class="col-md-12">
                                                      <div class="order-confirmation float-right">
                                                           <button type="submit" class="btn btn-outline-dark btn-lg btn-add-to-cart btn-add-food">
                                                                Add To Cart
                                                           </button>
                                                      </div>
                                                 </div>
                                            </div>
                                       </form>
                                  </div>
                             </div>
                        </div>
                   </div>
              </div>
         </div>
    </div>

    @endforeach
          <!-- ================================
               ========= Menu Modal Ends ===========
          ================================= -->




    <!-- ================================
             ========= Menu Modal Starts ===========
        ================================= -->
    @foreach ($packages as $key => $package)

    <div class="modal fade custom-modal" id="package_{{ $package->id }}" tabindex="-1" role="dialog" aria-labelledby="modalIdLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
              <div class="modal-content">
                   <div class="modal-header">
                        <h6 class="modal-title float-left" id="modalIdLabel">{{ $package->name }}</h6>
                        <div class="float-right d-flex">
                             <h6 class="modal-title  text-muted" id="modalIdPrice">Total Price: $@if($packagesOffer[$key]["price"]){{ $packagesOffer[$key]["price"] }} @else{{ $package->price }} @endif</h6>
                             <a href="#" class="btn btn-outline-dark btn-sm btn-modal-close" data-dismiss="modal" aria-label="Close">
                                  <i class="fas fa-times"></i>
                             </a>
                        </div>
                   </div>
                   <div class="modal-body">
                        <div class="row">
                             <div class="col-md-3">
                                  <div class="sl-no">
                                       @if($key+1 < 10){{ 0 }}@endif{{ $key+1  }}
                                  </div>
                             </div>

                             <div class="col-md-9">
                                  <div class="order-food-details">
                                       <p class="food-details">
                                            {{ $package->note }}
                                       </p>
                                       <form id="package_form_{{ $package->id }}">
                                            <input type="number" value="{{ $package->id }}" class="d-none">
                                            <div class="row">
                                                 <div class="col">
                                                 <h5 class="total-qn text-muted text-right" style="padding-top: 5px;">Total Quantity: </h5>
                                                 </div>
                                                 <div class="col">
                                                      <div class="quantity">
                                                           <input type="number" name="quantity" class="form-control quantity-input" min="1" max="100" step="1" value="1">
                                                      </div>
                                                 </div>

                                                 @isset($package->extra_food_list[0])
                                                 <div class="col-md-12">
                                                      <div class="extra-items">
                                                           <table class="table table-borderless">
                                                                <tbody>
                                                                     <tr class="extra-item-header">
                                                                          <th colspan="4">Extra Items</th>
                                                                     </tr>
                                                                     @foreach ($package->extra_food_list as $ex_food)
                                                                     <tr class="table-row" id="row_{{ $ex_food }}">
                                                                          <td class="select-or-not">
                                                                          <label class="custom-control custom-checkbox">
                                                                               <input name="status" type="checkbox" class="custom-control-input status" id="status_{{ $ex_food }}">
                                                                               <span class="custom-control-indicator"></span>
                                                                          </label>
                                                                          </td>
                                                                          <td class="modal-food-name-td">
                                                                               <h6>{{ App\Model\Backend\Food\Food::find($ex_food)->name }}</h6>
                                                                          </td>
                                                                          <td class="modal-cart-price-td">
                                                                               <?php
                                                                                    $offer = App\Model\Backend\Offer\Offer::where('food_ids','like','%'.$ex_food.'%')->where('exp_date', '>=', date('Y-m-d'))->orderBy('updated_at', 'desc')->first();
                                                                                    if($offer)
                                                                                    $price  =   number_format(App\Model\Backend\Food\Food::find($ex_food)->price - ( (App\Model\Backend\Food\Food::find($ex_food)->price * $offer->discount)/100 ), 2);
                                                                               ?>
                                                                               <h6>$@if($offer){{ $price }}@else{{ App\Model\Backend\Food\Food::find($ex_food)->price }}@endif</h6>
                                                                          </td>
                                                                          <td class="modal-quantity-cart-td" id="row_id_{{ $ex_food }}">
                                                                               <div class="quantity">
                                                                                    <input type="number" name="ex_quantity[{{ $ex_food }}]" class="form-control quantity-input extra-item-quantity" min="1" max="100" step="1" value="1" disabled id="extra_item_id_{{ $ex_food }}">
                                                                               </div>
                                                                          </td>
                                                                     </tr>
                                                                     @endforeach
                                                                </tbody>
                                                           </table>
                                                      </div>
                                                      </div>
                                                 @endisset

                                                 <div class="col-md-12">
                                                      <div class="notes-input">
                                                           <textarea class="form-control note-input" name="note" id="notePick" rows="3" placeholder="Your Extra Requirements"></textarea>
                                                      </div>
                                                 </div>
                                                 <div class="col-md-12">
                                                      <div class="order-confirmation float-right">
                                                           <button type="submit" class="btn btn-outline-dark btn-lg btn-add-to-cart btn-add-package">
                                                                Add To Cart
                                                           </button>
                                                      </div>
                                                 </div>
                                            </div>
                                       </form>
                                  </div>
                             </div>
                        </div>
                   </div>
              </div>
         </div>
    </div>

    @endforeach
          <!-- ================================
               ========= Menu Modal Ends ===========
          ================================= -->
