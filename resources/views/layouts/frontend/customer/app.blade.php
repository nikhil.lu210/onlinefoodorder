<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.frontend.partials.header')

    {{--  This will include all CSS files which are connected into header.blade.php in partials Folder  --}}
    @yield('stylesheet')

</head>
<body class="wrap-content">
    @include('layouts.frontend.partials.navbar')

        <div class="content" id="app_vue">
            @yield('content')
        </div>

    @include('layouts.frontend.partials.script')
    {{--  This will include all JS files which are connected into javascript.blade.php in partials Folder  --}}
</body>
</html>
