@extends('layouts.frontend.customer.app')

@section('page_title', '| Confirm Order')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .payment-part{
            padding-bottom: 70px;
        }
        .header h4{
            font-weight: 700;
            padding-top: 30px;
        }

        .order-type .header h4,
        .header.your-food-cart h4{
            padding-top: 70px;
        }
        .order-list{
            margin-top: 20px;
            border-radius: 10px;
            background-color: #ffffff;
            -webkit-box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
        }
        .add-cart-btn .btn-cart {
            height: 50px;
            width: 50px;
            border-color: #9999992e;
            padding: 12px;
            margin-top: 5px;
        }


        /* Order Type */
        .order-type-info{
            padding: 20px 20px;
            margin-top: 20px;
            border-radius: 10px;
            background-color: #ffffff;
            -webkit-box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
        }
        .pre-order,
        .post-order{
            display: none;
            margin-top: 20px;
        }
        .nice-select{
            line-height: 2.65 !important;
        }
        .nice-select .list{
            width: 100%;
        }
        .nice-select .option:hover {
            background-color: #fec62b;
        }

        .card-details .header h4{
            padding-top: 30px;
        }
        .card-info,
        .address-info{
            margin-top: 20px;
            border-radius: 10px;
            padding: 20px;
            background-color: #ffffff;
            -webkit-box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 2px 35px -8px rgba(0, 0, 0, 0.1);
        }
        label{
            font-weight: 600;
            color: #333333aa;
        }
        .form-control{
            font-weight: 700;
            width: 100%;
            height: calc(1.5em + .5rem + 10px);
            line-height: 1.65;
            float: left;
            display: block;
            padding: 0;
            margin: 0;
            padding-left: 10px;
            border: 1px solid #eee;
            margin-bottom: 15px;
        }
        .form-control:focus {
            border-color: #fec62b;
            outline: 0;
            box-shadow: none;
        }
        .exp-month::after{
            content: "/";
            position: absolute;
            right: -2px;
            top: 6px;
            color: #aaa;
            font-size: 18px;
        }
        .form-control::placeholder {
            opacity: 0.5; /* Firefox */
        }

        .form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
            opacity: 0.5;
        }

        .form-control::-ms-input-placeholder { /* Microsoft Edge */
            opacity: 0.5;
        }

        .btn-confirm-order{
            height: calc(1.5em + .5rem + 20px);
            width: 90%;
            font-size: 18px;
            text-align: center;
        }
        .confirm-btn .btn-confirm-order{
            border-radius: 30px;
            font-weight: 900;
            font-size: 14px;
            background-color: #FEC62B;
            border-color: #FEC62B;
            transition: 0.3s all ease-in-out;
        }
        .confirm-btn .btn-confirm-order:hover{
            box-shadow: 0px 0px 30px -3px rgba(0, 0, 0, 0.15);
            transition: 0.3s all ease-in-out;
        }
        .confirm-btn .btn-confirm-order:focus,
        .confirm-btn .btn-confirm-order:active,
        .confirm-btn .btn-confirm-order:not(:disabled):not(.disabled).active,
        .confirm-btn .btn-confirm-order:not(:disabled):not(.disabled):active,
        .show>.confirm-btn .btn-confirm-order.dropdown-toggle{
            background-color: #FEC62B;
            border: 1px solid #FEC62B;
            box-shadow: none !important;
            outline: none !important;
            transition: 0.3s all ease-in-out;
        }
        .confirm-btn .btn-confirm-order.disabled,
        .confirm-btn .btn-confirm-order.disabled:hover,
        .confirm-btn .btn-confirm-order.disabled:focus,
        .confirm-btn .btn-confirm-order.disabled:active,
        .confirm-btn .btn-confirm-order:disabled{
            background-color: #dddddd;
            border: 1px solid #dddddd;
            color: #333333;
            box-shadow: none;
            cursor: not-allowed;
        }
        .bootstrap-select.form-control{
            margin-bottom: 15px;
        }
        .cart-btn-div{
            display: none !important;
        }

        .btn-coupon{
            padding: 8px 10px;
        }
        .coupon-tr td{
            padding-top: 25px;
        }

        @media (max-width: 575.98px) and (min-width: 320px){
            .add-cart-btn .btn-cart {
                height: 30px;
                width: 30px;
                border-color: #9999992e;
                padding: 6px;
                font-size: 12px;
                margin-top: 2px;
            }
        }
    </style>
@endsection


@section('content')
{{-- Body Parts From Here --}}
<!-- ================================
    ========= Carousel Part Starts ===========
================================= -->

<section class="carousel-part">
    <div class="overlay">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-12">
                    <div class="content text-center">
                        <h1 class="heading">Cash On Delivery</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ================================
========= Carousel Part Ends ===========
================================= -->



<!-- ================================
    ========= payment Part Starts ===========
================================= -->

<section class="payment-part cash">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <form action="{{ route('customer.order.cash.store') }}" method="POST">
                @csrf

                    {{-- =======================================================
                    ====================< Order Type Starts >===================
                    ======================================================== --}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="order-type">
                                <div class="header">
                                    <h4>Order Type</h4>
                                </div>

                                <div class="order-type-info">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="custom-control custom-checkbox">
                                                <input name="order_type" value="1" id="postOrder" type="checkbox" class="custom-control-input status {{ $errors->has('order_type') ? ' is-invalid' : '' }}">
                                                @if ($errors->has('order_type'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('order_type') }}</strong>
                                                    </span>
                                                @endif
                                                <span class="custom-control-indicator"></span> Post-Order
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="custom-control custom-checkbox">
                                                <input name="order_type" value="2" id="preOrder" type="checkbox" class="custom-control-input status {{ $errors->has('order_type') ? ' is-invalid' : '' }}">
                                                @if ($errors->has('order_type'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('order_type') }}</strong>
                                                    </span>
                                                @endif
                                                <span class="custom-control-indicator"></span> Pre Order
                                            </label>
                                        </div>

                                        <div class="col-12">
                                            <div class="post-order">
                                                <label for="time">Time *</label>
                                                <select name="pre_time" id="timePick" class="form-control nice-select {{ $errors->has('pre_time') ? ' is-invalid' : '' }}">
                                                    <option selected value="">Select Time</option>
                                                    <option value="01:30:10">01:30 PM</option>
                                                    <option value="01:40:30">01:40 PM</option>
                                                </select>
                                                @if ($errors->has('pre_time'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('pre_time') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="pre-order">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <label for="datePick">Date *</label>
                                                        <input class="form-control form-control-sm {{ $errors->has('post_date') ? ' is-invalid' : '' }}"  id="datePick" type="text" name="post_date" placeholder="01-02-2019">
                                                        @if ($errors->has('post_date'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('post_date') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-12">
                                                        <label for="time">Time *</label>
                                                        <select name="post_time" id="timePick" class="form-control nice-select {{ $errors->has('post_time') ? ' is-invalid' : '' }}">
                                                            <option selected value="">Select Time</option>
                                                            <option value="01:30:00">01:30PM</option>
                                                            <option value="01:40:00">01:40PM</option>
                                                        </select>
                                                        @if ($errors->has('post_time'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('post_time') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- =======================================================
                    =====================< Order Type Ends >====================
                    ======================================================== --}}


                    {{-- =======================================================
                    ====================< Address Part Starts >=================
                    ======================================================== --}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="address-details">
                                <div class="header">
                                    <h4>Your Address</h4>
                                </div>

                                <div class="address-info">
                                    <div class="row">
                                        <div class="col-12">
                                            <label for="fullName">Full Name *</label>
                                            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="fullName" name="name" placeholder="{{ Auth::user()->name }}" value="{{ Auth::user()->name }}" required>
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                            <label for="phoneNumber">Phone Number *</label>
                                            <input type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phoneNumber" name="phone" placeholder="12345678900" required>
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                            <label for="country">Country *</label>
                                            <select name="country" id="country" class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" data-live-search="true" required>
                                                <option value="">Select Country</option>
                                            </select>
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="col-12">
                                            <label for="city">City *</label>
                                            <select name="city" id="state" class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" data-live-search="true" required>
                                                <option value="">Select City</option>
                                            </select>
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                            <label for="area">Area *</label>
                                            <input type="text" class="form-control {{ $errors->has('area') ? ' is-invalid' : '' }}" id="area" name="area" placeholder="Chelsea" required>
                                            @if ($errors->has('area'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('area') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                            <label for="address">Address *</label>
                                            <textarea class="form-control address-input {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="addressPick" rows="3" placeholder="Address" required></textarea>
                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- =======================================================
                    =====================< Address Part Ends >==================
                    ======================================================== --}}

                    <div class="row">
                        <div class="col-12">
                            <div class="confirm-btn text-center">
                                <button class="btn btn-dark btn-confirm-order btn-lg" type="submit">Confirm Order</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>








            <div class="col-md-6">

                {{-- =======================================================
                =====================< Cart Part Starts >===================
                ======================================================== --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="header your-food-cart">
                            <h4>Your Food Cart</h4>
                        </div>
                        <div class="order-list">
                            <div class="tab-content">
                                <div class="tab-pane fade show active">
                                    <table class="table table-borderless" id="menuTable">
                                        <tbody>
                                            <?php $total = 0; $show_total=0; ?>
                                            @if ($orders)

                                            @foreach ($orders as $key => $order)

                                                <tr class="h-100">
                                                    <td class="sl-td h-100 justify-content-center align-items-center">
                                                        <div class="sl-no">{{ $order["quantity"] }}</div>
                                                    </td>
                                                    <td class="food-info-td">
                                                        <div class="food-info">
                                                            <?php
                                                                $name = ($order["food_package"] == "food") ? App\Model\Backend\Food\Food::find($order["id"])->name:
                                                                App\Model\Backend\Package\Package::find($order["id"])->name;
                                                                $total += $order["original_price"];
                                                                $show_total += $order["price"]
                                                           ?>
                                                           <a href="#" class="food-name" >{{ $name }}</a>
                                                            <?php
                                                                if($order["ex_quantity"]){
                                                                    $string = "";
                                                                    $arr = (array) $order["ex_quantity"];
                                                                    foreach($arr as $list => $ex_order){
                                                                        $string .=  App\Model\Backend\Food\Food::find($list)->name . " (". $ex_order .") | ";
                                                                    }
                                                                }
                                                            ?>
                                                            @if($order["ex_quantity"])
                                                                <div class="info">{{ $string }}</div>
                                                            @else
                                                                <div class="info">{{ $order["note"] }}</div>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td class="price-td">
                                                        <div class="price">
                                                                <h4>$@if($setedCoupon){{ $order["original_price"] }}@else{{ $order["price"] }}@endif</h4>
                                                        </div>
                                                    </td>
                                                    <td class="add-cart-td">
                                                        <div class="add-cart-btn cash-cart-btn">
                                                            <button id="remove_cart_cash_{{ $key }}" class="btn btn-outline-dark btn-cart">
                                                                <i class="fas fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            @endif



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- =======================================================
                ======================< Cart Part Ends >====================
                ======================================================== --}}




                {{-- =======================================================
                =====================< Coupon Part Starts >===================
                ======================================================== --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="header">
                            <h4>Coupon</h4>
                            <div class="info">If you Remove / Update / Add any Food in order list, and if you have any Coupon Code, Then you must have to apply it again after updating your cart.</div>
                        </div>
                        <div class="order-list">
                            <div class="tab-content">
                                <div class="tab-pane fade show active">
                                    <table class="table table-borderless" id="menuTable">
                                        <tbody>
                                            <form id="set-coupon-web-cash">
                                                <tr class="coupon-tr">
                                                        <td colspan="3" class="coupon-input-td"><input class="form-control form-control-sm coupon-input" id="set-coupon-input-web-cash" type="text" placeholder="Coupon Code" name="coupon_code" required></td>
                                                        <td class="coupon-btn-td"><button class="btn btn-outline-dark btn-sm btn-block btn-coupon btn-coupon-web-cash"><i class="fas fa-angle-right"></i></button></td>
                                                </tr>
                                            </form>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- =======================================================
                ======================< Coupon Part Ends >====================
                ======================================================== --}}




                {{-- =======================================================
                =====================< Price Part Starts >===================
                ======================================================== --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="header">
                            <h4>Your Order Price Details</h4>
                        </div>
                        <div class="order-list">
                            <div class="tab-content">
                                <div class="tab-pane fade show active">
                                    <table class="table table-borderless" id="menuTable">
                                        <tbody>
                                            <tr class="h-100">
                                                <td colspan="2"><strong>Total Price: </strong></td>
                                                <td class="text-right"><strong>$@if($setedCoupon){{ number_format($total, 2) }}@else{{ number_format($show_total, 2) }}@endif</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>Total Discount: </strong></td>
                                                <td class="text-right"><strong>$@if($setedCoupon){{ number_format($setedCoupon["totalDiscount"], 2) }}@else{{ 0.00 }}@endif</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>Discount Price: </strong></td>
                                                <td class="text-right"><strong>$@if($setedCoupon){{ number_format($setedCoupon["reducedPrice"], 2) }}@else{{ number_format($show_total, 2) }}@endif</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- =======================================================
                ======================< Price Part Ends >====================
                ======================================================== --}}




            </div>

        </div>
    </div>
</section>

<!-- ================================
========= payment Part Ends ===========
================================= -->
@endsection


@section('scripts')
    {{--  External Javascript  --}}
    <script type= "text/javascript" src = "{{ asset('frontend/js/countries.js') }}"></script>
    <script>
        $(document).ready(function() {
            // Order Type Part
            $('input').on('click',function () {
                var pre = $('#preOrder');
                var post = $('#postOrder');

                // Preorder
                if (pre.is(':checked')) {
                    post.attr('disabled', 'disabled');
                }else {
                    post.removeAttr('disabled', 'disabled');
                }

                // Postorder
                if (post.is(':checked')) {
                    pre.attr('disabled', 'disabled');
                }else {
                    pre.removeAttr('disabled', 'disabled');
                }
            });

            $('#preOrder').on('click', function(){
                $('.pre-order').slideToggle();
            });

            $('#postOrder').on('click', function(){
                $('.post-order').slideToggle();
            });
        });

        // ================ for getting country and state name ===============

        populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is

        // ================ for getting country and state name ===============

        $(".cash-cart-btn button").click(function(e){
            e.preventDefault();
            var confirm = ConfirmDelete();
            if(!confirm) return;
            var address = $(this).attr('id');
            var id = address.substring(17, address.length);

            $.ajax({
                method: 'GET',
                url: '/cart/destroyOrderCookies/'+id ,
                datatype: "JSON",
                success: function(data) {
                    window.location.replace(document.location.origin+'/customer/order/cash');
                },
                error: function() {
                    console.log("do not destory from cart");
                }
            });

        });

        $("button.btn-coupon-web-cash").click(function(e){
            e.preventDefault();
            var form = $("#set-coupon-web-cash");
            var formData = $(form).serialize();
            $("#set-coupon-input-web-cash").val('');
            $.ajax({
                method: 'POST',
                url: '/cart/calculateCoupon' ,
                data: formData,
                datatype: "JSON",
                success: function(data) {
                    if(data == 'login') window.location.replace(document.location.origin+'/login');
                    if(data[0] == false && data[1] == "no_coupon") alert("This Coupon is not avialable now!!!");
                    else if(data[0] == false && data[1] == "min_purchase") alert("You must Take more than "+data[2]+" cost!!!");
                    else if(data[0] == false && data[1] == "use_limit") alert("You already cross uses limit for this coupon!!!");
                    else window.location.replace(document.location.origin+'/customer/order/cash');
                },
                error: function() {
                    console.log("do not add to cart");
                }
            });

        });



    </script>
@endsection
