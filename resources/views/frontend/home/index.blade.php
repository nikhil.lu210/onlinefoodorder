
@extends('layouts.frontend.customer.app')

@section('page_title', '| Food Order')

@section('stylesheet')
    {{--  External CSS  --}}

     <style>
          sup.discount-offer {
               font-size: 10px;
               top: -1em;
               font-weight: 800;
               color: #fe2b2b;
          }
          sup.discount-offer-price {
               color: #ff2626;
               text-decoration: line-through;
          }
          sup.discount-offer-price span{
               font-size: 12px;
               top: -1em;
               font-weight: 800;
               color: #c1c1c1;
          }
     </style>
@endsection

@section('content')
{{-- Body Parts From Here --}}
    <!-- ================================
     ========= Body Starts ===========
    ================================= -->

    <section class="carousel-part">
        <div class="overlay">
              <div class="container h-100">
                   <div class="row h-100 justify-content-center align-items-center">
                        <div class="col-md-12">
                             <div class="content text-center">
                                  <h1 class="heading">ONLINE FOOD ORDER</h1>
                             </div>
                        </div>
                   </div>
              </div>
        </div>
   </section>

   <!-- ================================
    ========= Body Ends ===========
   ================================= -->


   <!-- ================================
        ========= Opening Hour Starts ===========
   ================================= -->

   <section class="body-part">
         <div class="container h-100">
              <div class="row h-100 justify-content-center align-items-center">
                   <div class="col-md-6">
                        <div class="opening-hr-div text-center">
                             <p class="open-hr">
                                  <span>Opening Hour: </span>
                                  <span>Monday to Friday at</span>
                                  <span>06.00am to 11.00pm</span>
                             </p>
                        </div>
                   </div>
              </div>
         </div>
    </section>

   <!-- ================================
        ========= Opening Hour Ends ===========
   ================================= -->


   <!-- ================================
        ========= Search Starts ===========
   ================================= -->
    <section class="search-part">
         <div class="container">
              <div class="row">
                   <div class="col-md-12">
                        <form action="" method="post">
                             <div class="d-flex justify-content-center h-100">
                                  <div class="searchbar">
                                    <input class="search_input" type="text" name="searchInput" id="searchInput" placeholder="Search...">
                                    <a href="#menuTable" class="search_icon"><i class="fas fa-search"></i></a>
                                  </div>
                                </div>
                        </form>
                   </div>
              </div>
         </div>
    </section>
   <!-- ================================
        ========= Search Ends ===========
   ================================= -->


   <!-- ================================
        ========= Order Part Starts ===========
   ================================= -->
    <section class="order-part">
         <div class="container">
              <div class="row">

                   <!-- Small Device Categories -->
                   <div class="col-12">
                        <h6 class="float-left f-b-cat">Find By Category</h6>
                        <button class="btn btn-outline-dark btn-sm btn-category float-right"><i class="fas fa-bars"></i></button>

                        <div id="mySidenav" class="sidenav">
                             <a href="javascript:void(0)" class="closebtn">&times;</a>

                             @foreach ($categories as $category)
                                   <a href="#{{ $category->name.$category->id }}" class="clsbtn active">{{ $category->name }}</a>
                             @endforeach
                        </div>
                   </div>

                   <!-- Categories -->
                   <div class="col-md-3">
                        <div class="categories-part">
                             <div class="header">
                                  <h4>Categories</h4>
                             </div>
                             <div class="categories">
                                  <ul class="nav nav-tabs d-block" id="nav-tab" role="tablist">
                                        @foreach ($categories as $key => $category)
                                             <li>
                                                  <a class="nav-item nav-link @if($key == 0) {{ 'active' }}@endif" href="#{{ $category->name.$category->id }}">{{ $category->name }}</a>
                                             </li>
                                        @endforeach

                                        @if ( isset($packages[0]) )
                                             <li>
                                                  <a class="nav-item nav-link" href="#package">Package</a>
                                             </li>
                                        @endif
                                  </ul>
                             </div>
                        </div>
                   </div>

                   <!-- Menus -->
                   <div class="col-md-6">
                        <div class="menus-part">
                             <div class="menus">
                                  <div class="tab-content">
                                       <div class="tab-pane fade show active">
                                            <table class="table table-borderless" id="menuTable">
                                                 <tbody>
                                                      @foreach ($categories as $category)
                                                            <tr class="category-devider text-center" id="{{ $category->name.$category->id }}">
                                                                 <td class="cat-name" colspan="4">{{ $category->name }}</td>
                                                            </tr>
                                                            <?php $key = 0;?>
                                                            @foreach ($foods as $index => $food)
                                                                 @if ($food->cat_id == $category->id)
                                                                      <tr class="h-100">
                                                                           <td class="sl-td h-100 justify-content-center align-items-center">
                                                                                <div class="sl-no"> @if($key+1 < 10){{ 0 }}<?php $key++; ?>@endif{{ $key  }} </div>
                                                                           </td>
                                                                           <td class="food-info-td">
                                                                                <div class="food-info">

                                                                                     <a href="#" data-toggle="modal" data-target="#food_{{ $food->id.$food->cat_id }}" class="food-name">
                                                                                          {{ $food->name }}
                                                                                          @if($foodsOffer[$index]["discount"])<sup class="discount-offer">({{ $foodsOffer[$index]["discount"] }}% Discount)</sup> @endif
                                                                                     </a>
                                                                                     <div class="info">{{ $food->note }}</div>
                                                                                </div>
                                                                           </td>
                                                                           <td class="price-td">
                                                                                <div class="price">
                                                                                     <h4>
                                                                                          @if($foodsOffer[$index]["discount"])
                                                                                          ${{ number_format( $foodsOffer[$index]["price"], 2) }}
                                                                                          <sup class="discount-offer-price">
                                                                                               <span>${{ number_format($food->price, 2) }}</span>
                                                                                          </sup>
                                                                                          @else
                                                                                          ${{ $food->price }}
                                                                                          @endif

                                                                                     </h4>
                                                                                </div>
                                                                           </td>
                                                                           <td class="add-cart-td">
                                                                                <div class="add-cart-btn">
                                                                                     <a href="#" class="btn btn-outline-dark btn-cart" data-toggle="modal" data-target="#food_{{ $food->id.$food->cat_id }}"><i class="fas fa-cart-plus"></i></a>
                                                                                </div>
                                                                           </td>
                                                                      </tr>
                                                                 @endif
                                                            @endforeach
                                                      @endforeach

                                                      @if ( isset($packages[0]) )
                                                            <tr class="category-devider text-center" id="package">
                                                                 <td class="cat-name" colspan="4">package</td>
                                                            </tr>
                                                            @foreach ($packages as $key => $package)
                                                                 <tr class="h-100">
                                                                      <td class="sl-td h-100 justify-content-center align-items-center">
                                                                           <div class="sl-no"> @if($key+1 < 10){{ 0 }}@endif{{ $key+1  }} </div>
                                                                      </td>
                                                                      <td class="food-info-td">
                                                                           <div class="food-info">

                                                                                <a href="#" data-toggle="modal" data-target="#package_{{ $package->id }}" class="food-name">
                                                                                     {{ $package->name }}
                                                                                     @if($packagesOffer[$key]["discount"])<sup class="discount-offer">({{ $packagesOffer[$key]["discount"] }}% Discount)</sup> @endif
                                                                                </a>

                                                                                <div class="info">{{ $package->note }}</div>
                                                                           </div>
                                                                      </td>
                                                                      <td class="price-td">
                                                                           <div class="price">
                                                                                <h4>
                                                                                     @if($packagesOffer[$key]["discount"])
                                                                                     ${{ number_format($packagesOffer[$key]["price"], 2) }}
                                                                                     <sup class="discount-offer-price">
                                                                                          <span>${{ number_format($package->price, 2) }}</span>
                                                                                     </sup>
                                                                                     @else
                                                                                     ${{ $package->price }}
                                                                                     @endif
                                                                                </h4>
                                                                           </div>
                                                                      </td>
                                                                      <td class="add-cart-td">
                                                                           <div class="add-cart-btn">
                                                                                <a href="#" class="btn btn-outline-dark btn-cart" data-toggle="modal" data-target="#package_{{ $package->id }}"><i class="fas fa-cart-plus"></i></a>
                                                                           </div>
                                                                      </td>
                                                                 </tr>
                                                            @endforeach
                                                       @endif

                                                 </tbody>
                                            </table>
                                       </div>
                                  </div>
                             </div>
                        </div>
                   </div>

                   <!-- Cart -->
                   <div class="col-md-3">
                        <div class="cart-part">
                             <div class="header">
                                  <h4>Your Orders</h4>
                             </div>
                             <div class="cart-info">
                                  <div class="items">
                                       <table class="table table-borderless">
                                            <tbody class="cart-items" id="cart_items">
                                                 <?php $total = 0; $show_total=0; ?>
                                                 @foreach ($orders as $key => $order)
                                                       <tr class="h-100">
                                                            <td class="total-item-td h-100 justify-content-center align-items-center">
                                                                 <div class="total-item">{{ $order["quantity"] }}</div>
                                                            </td>
                                                            <td class="food-name-td">
                                                                 <?php
                                                                      $name = ($order["food_package"] == "food") ? App\Model\Backend\Food\Food::find($order["id"])->name:
                                                                      App\Model\Backend\Package\Package::find($order["id"])->name;
                                                                      $total += $order["original_price"];
                                                                      $show_total += $order["price"]
                                                                 ?>
                                                                 <a href="#" class="food-name"  data-toggle="modal" data-target="#order_{{ $order["id"].$order["food_package"] }}">{{ $name }}</a>
                                                            </td>
                                                            <td class="cart-price-td">
                                                                 <div class="cart-price">
                                                                      <h6>$@if($setedCoupon){{ $order["original_price"] }}@else{{ $order["price"] }}@endif</h6>
                                                                 </div>
                                                            </td>
                                                            <td class="remove-cart-td">
                                                                 <div class="remove-cart-btn">
                                                                 <button id="remove_cart_{{ $key }}" class="btn btn-outline-dark btn-sm btn-remove-cart" onclick="return ConfirmDelete();"><i class="fas fa-minus"></i></button>
                                                                 </div>
                                                            </td>
                                                       </tr>
                                                 @endforeach
                                            </tbody>
                                            <tbody>
                                                <form id="set-coupon-web">
                                                    <tr class="coupon-tr">
                                                            <td colspan="3" class="coupon-input-td"><input class="form-control form-control-sm coupon-input" id="set-coupon-input-web" type="text" placeholder="Coupon Code" name="coupon_code" required></td>
                                                            <td class="coupon-btn-td"><button class="btn btn-outline-dark btn-sm btn-block btn-coupon btn-coupon-web"><i class="fas fa-angle-right"></i></button></td>
                                                    </tr>
                                                </form>

                                            </tbody>
                                       </table>

                                       <table class="table table-borderless">
                                            <tbody>
                                                 <tr>
                                                      <td colspan="2"><strong>Total Price: </strong></td>
                                                      <td class="text-right" id="total_price"><strong>$@if($setedCoupon){{ number_format($total, 2) }}@else{{ number_format($show_total, 2) }}@endif</strong></td>
                                                 </tr>
                                                 <tr>
                                                      <td colspan="2"><strong>Total Discount: </strong></td>
                                                      <td class="text-right" id="total_discount"><strong>$@if($setedCoupon){{ number_format($setedCoupon["totalDiscount"], 2) }}@else{{ 0.00 }}@endif</strong></td>
                                                 </tr>
                                                 <tr id="app_id">
                                                      <td colspan="2"><strong>Discount Price: </strong></td>
                                                      <td class="text-right" id="after_discount"><strong>$@if($setedCoupon){{ number_format($setedCoupon["reducedPrice"], 2) }}@else{{ number_format($show_total, 2) }}@endif</strong></td>

                                                 </tr>
                                            </tbody>
                                       </table>

                                       <form action="{{ route('customer.home.confirmation') }}" method="post">
                                        @csrf
                                            <div class="row food-receive">
                                                <div class="col-md-6 text-left">
                                                    <div class="form-check">
                                                        <input class="form-check-input pickup-input" type="radio" value="1" id="pickup" name="delivery_type">
                                                        <label class="form-check-label pickup" for="pickup">
                                                            Pickup
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-left">
                                                    <div class="form-check">
                                                        <input class="form-check-input delivery-input" type="radio" value="2" id="delivery" name="delivery_type">
                                                        <label class="form-check-label delivery" for="delivery">
                                                            Delivery
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row food-payment">
                                                <div class="col-md-6 text-left">
                                                    <div class="form-check">
                                                        <input class="form-check-input cashon-input" type="radio" value="1" id="cashon" name="payment_type">
                                                        <label class="form-check-label cashon" for="cashon">
                                                            Cashon
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-left">
                                                    <div class="form-check">
                                                        <input class="form-check-input digital-input" type="radio" value="2" id="digital" name="payment_type">
                                                        <label class="form-check-label digital" for="digital">
                                                            VCard
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row h-100 justify-content-center align-items-center confirm-btn">
                                                <div class="col-md-10">
                                                    <button class="btn btn-dark btn-confirm btn-block btn-display btn-lg" disabled>Confirm Order</button>

                                                    <button type="submit" class="btn btn-dark btn-confirm btn-cashon-vcard btn-block btn-lg d-none">Confirm Order</button>
                                                </div>
                                            </div>
                                        </form>
                                  </div>
                             </div>
                        </div>
                   </div>

              </div>
         </div>
    </section>

    @include('layouts.frontend.partials.modals')
   <!-- ================================
        ========= Order Part Ends ===========
   ================================= -->
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    {{-- <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script type="text/javascript">
        new Vue({
         el : '#app_id',
         data() {
            a:'sabbir ahmed',
         },
         methods: {
           hello: function() {
             console.log('hello');
           }
         },
         mounted() {
            // this.hello()
        },
        ready() {

        }
       });
   </script> --}}
@endsection
