@extends('layouts.backend.super_admin.app')

@section('page_title', '| Dashboard')

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')
<header class="page-header">
    <h2><b>Dashboard</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="dashboard-part">
    <div class="row">
        <div class="col-md-6 col-lg-12 col-xl-6">
            <div class="row">

                <div class="col-md-4">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-users"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Total Customer</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $customer }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-cogs"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Total Admin</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $admin }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-male"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Total Delivery Boy</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $deliBoy }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-bell-o"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Total Order</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $totOrder }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-spinner"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Pending Order</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $penOrder }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-refresh"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Processing Order</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $proOrder }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-times"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Canceled Order</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{ $canOrder }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
    {{--  External Javascript  --}}

@endsection
