@extends('layouts.backend.super_admin.app')

@section('page_title', '| All Orders')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}
.icon-process{
    font-size: 20px;
}
.color-done{
    color: #44f100;
}
.color-processing{
    color: #fec62b;
    transform: rotateY(180deg);
}
.color-cancel{
    color: #fe2b2b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>All Orders</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Records</span></li>
            <li><span>All Orders</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Orders</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Received By</th>
                    <th>Delivery Boy</th>
                    <th class="text-center">Process</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr class="gradeX">
                    <td>12-12-2019</td>
                    <td>03.00</td>
                    <td>Customer Name</td>
                    <td>$120</td>
                    <td>Admin Name</td>
                    <td>Delivery Boy Name</td>
                    <td class="text-center">
                        <i class="fa fa-check icon-process color-done"></i>
                        {{-- <i class="fa fa-history icon-process color-processing"></i>
                        <i class="fa fa-times icon-process color-cancel"></i> --}}
                    </td>
                    <td class="action-td">
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                        <a href="{{ Route('superadmin.record.all.order.show', 1) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>

                <tr class="gradeX">
                    <td>12-12-2019</td>
                    <td>03.00</td>
                    <td>Customer Name</td>
                    <td>$120</td>
                    <td>Admin Name</td>
                    <td>Delivery Boy Name</td>
                    <td class="text-center">
                        <i class="fa fa-history icon-process color-processing"></i>
                    </td>
                    <td class="action-td">
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>

                <tr class="gradeX">
                    <td>12-12-2019</td>
                    <td>03.00</td>
                    <td>Customer Name</td>
                    <td>$120</td>
                    <td>Admin Name</td>
                    <td>Delivery Boy Name</td>
                    <td class="text-center">
                        <i class="fa fa-times icon-process color-cancel"></i>
                    </td>
                    <td class="action-td">
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


<script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>
@endsection
