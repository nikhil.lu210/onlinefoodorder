@extends('layouts.backend.super_admin.app')

@section('page_title', '| Pending Orders | Details')

@section('stylesheet_links')
{{-- External CSS Links --}}

@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
    .panel-actions{
        top: 13px;
    }

    .table.table-bordered tbody tr th{
        width: 30% !important;
    }
    .table.table-bordered tbody tr td{
        width: 70% !important;
    }
    ul, ol {
        padding-left: 15px;
    }

    .menu-name{
        font-weight: 600;
    }
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Pending Order Details</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Records</span></li>
            <li>
                <a href="{{ Route('superadmin.record.pending.order') }}">
                    Pending Orders
                </a>
            </li>
            <li><span>Details</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <button class="btn btn-default btn-sm" onclick="window.history.go(-1); return false;">Back</button>
        </div>
        <h2 class="panel-title">Customer Name</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped table-hover">
            <tbody>

                {{-- Customer Name --}}
                <tr>
                    <th>Customer Name :</th>
                    <td>Jhon Doe</td>
                </tr>

                {{-- Order Submission Date --}}
                <tr>
                    <th>Order Submission Date :</th>
                    <td>20-04-2019</td>
                </tr>

                {{-- Order Submission Time --}}
                <tr>
                    <th>Order Submission Time :</th>
                    <td>02:30 PM</td>
                </tr>

                {{-- Expected Delivery Date --}}
                <tr>
                    <th>Expected Delivery Date :</th>
                    <td>20-04-2019</td>
                </tr>

                {{-- Expected Delivery Time --}}
                <tr>
                    <th>Expected Delivery Time :</th>
                    <td>02:50 PM</td>
                </tr>

                {{-- Ordered Menus --}}
                <tr>
                    <th>Ordered Menus :</th>
                    <td>
                        <ol>
                            <li>
                                <span class="menu-name">Chicken Tikka</span>
                            </li>
                            <li>
                                <span class="menu-name">Chicken Grill</span>
                            </li>
                        </ol>
                    </td>
                </tr>

                {{-- Total Price --}}
                <tr>
                    <th>Total Price :</th>
                    <td>$1200</td>
                </tr>

                {{-- Note / Comments --}}
                <tr>
                    <th>Note / Comments :</th>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus qui impedit tenetur eligendi commodi ducimus quidem adipisci temporibus voluptatum, dolor ut architecto eveniet iure quo omnis, doloremque atque dolorum. Architecto quae laboriosam aspernatur ad voluptas at perferendis quod ratione reprehenderit nam, fugit ea facilis culpa, vero earum maxime, natus saepe?</td>
                </tr>

            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}

@endsection
