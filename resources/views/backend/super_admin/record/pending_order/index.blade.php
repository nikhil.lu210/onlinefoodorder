@extends('layouts.backend.super_admin.app')

@section('page_title', '| Pending Orders')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Pending Orders</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Records</span></li>
            <li><span>Pending Orders</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        {{-- <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
        </div> --}}

        <h2 class="panel-title">Pending Orders</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>Order Date</th>
                    <th>Order Time</th>
                    <th>Expected Date</th>
                    <th>Expected Time</th>
                    <th>Name</th>
                    <th>Total Item</th>
                    <th>Total Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr class="gradeX">
                    <td>12-12-2019</td>
                    <td>03.00</td>
                    <td>12-12-2019</td>
                    <td>03.00</td>
                    <td>Jhon Doe</td>
                    <td>8</td>
                    <td>1200</td>
                    <td class="action-td">
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                        <a href="{{ Route('superadmin.record.pending.order.show', 1) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


<script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>
@endsection
