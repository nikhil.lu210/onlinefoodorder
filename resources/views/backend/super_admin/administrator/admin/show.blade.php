@extends('layouts.backend.super_admin.app')

@section('page_title', '| Admin Name')

@section('stylesheet_links')
{{-- External CSS Links --}}

@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
    .panel-actions{
        top: 13px;
    }

    .table.table-bordered tbody tr th{
        width: 30% !important;
    }
    .table.table-bordered tbody tr td{
        width: 70% !important;
    }
    ul, ol {
        padding-left: 15px;
    }

    .menu-name{
        font-weight: 600;
    }
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Admin Details</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Administrator</span></li>
            <li>
                <a href="{{ Route('superadmin.administrator.admin') }}">
                    All Admin
                </a>
            </li>
            <li><span>Admin Name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <button class="btn btn-default btn-sm" onclick="window.history.go(-1); return false;">Back</button>
        </div>
        <h2 class="panel-title">Admin Name</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            {{-- Profile Avatar --}}
            <div class="col-md-5">
                <img src="{{ asset('backend/images/profile.png') }}" alt="" class="img-responsive img-thumbnail">
            </div>

            {{-- Profile Details --}}
            <div class="col-md-7">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>

                        {{-- Admin Name --}}
                        <tr>
                            <th>Name :</th>
                            <td>Admin Name</td>
                        </tr>

                        {{-- Admin Number --}}
                        <tr>
                            <th>Phone No :</th>
                            <td>+8801234678768</td>
                        </tr>

                        {{-- Admin Email --}}
                        <tr>
                            <th>Eamil :</th>
                            <td>admin@mail.com</td>
                        </tr>

                        {{-- Note / Comments --}}
                        <tr>
                            <th>Address :</th>
                            <td>
                                House No 201/B,
                                City Name, Country
                            </td>
                        </tr>

                        {{-- Total Order Confirmed --}}
                        <tr>
                            <th>Order Confirmed :</th>
                            <td>200</td>
                        </tr>

                        {{-- Total Order Canceled --}}
                        <tr>
                            <th>Order Canceled :</th>
                            <td>32</td>
                        </tr>

                        {{-- Administartor Since --}}
                        <tr>
                            <th>Administartor Since :</th>
                            <td>12-12-2019</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}

@endsection
