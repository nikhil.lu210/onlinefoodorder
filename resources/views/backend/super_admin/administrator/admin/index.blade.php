@extends('layouts.backend.super_admin.app')

@section('page_title', '| All Admins')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}

tr>td{
    padding-top: 25px !important;
}
.avatar-td{
    max-width: 5% !important;
    padding: 5px 0px !important;
}
.avatar-td img.img-responsive.img-thumbnail{
    max-width: 70px !important;
    border-radius: 0px;
}
td.action-td {
    padding-top: 20px !important;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>All Admins</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Administrator</span></li>
            <li><span>All Admins</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Admins</h2>
    </header>

    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>SL.</th>
                    <th class="text-center">Avatar</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr class="gradeX">
                    <td>01</td>
                    <td class="avatar-td text-center">
                        <img src="{{ asset('backend/images/profile.png') }}" alt="Image" class="img-responsive img-thumbnail">
                    </td>
                    <td>Jhon Doe</td>
                    <td>+8801712345678</td>
                    <td>jhondoe@mail.com</td>
                    <td class="action-td">
                        <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                        <a href="{{ Route('superadmin.administrator.admin.show', 1) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


<script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>
@endsection
