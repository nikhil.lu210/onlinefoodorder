@extends('layouts.backend.super_admin.app')

@section('page_title', '| All Admins')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/pnotify/pnotify.custom.css') }}" />

@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}

@media (min-width: 768px){
    .form-horizontal .control-label {
        text-align: right;
        margin-bottom: 0;
        padding-top: 3px;
    }
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>All Admins</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Administrator</span></li>
            <li>
                <a href="{{ Route('superadmin.administrator.admin') }}">
                    Admins
                </a>
            </li>
            <li><span>Assign New Admin</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-2">
                <header class="panel-heading">
                    <h2 class="panel-title">Assign New Admin</h2>
                </header>
                <div class="panel-body panel-body-nopadding">
                    <div class="wizard-tabs">
                        <ul class="wizard-steps">
                            <li class="active">
                                <a href="#w1-account" data-toggle="tab" class="text-center">
                                    <span class="badge hidden-xs">1</span> Account
                                </a>
                            </li>
                            <li>
                                <a href="#w1-profile" data-toggle="tab" class="text-center">
                                    <span class="badge hidden-xs">2</span> Profile
                                </a>
                            </li>
                            <li>
                                <a href="#w1-confirm" data-toggle="tab" class="text-center">
                                    <span class="badge hidden-xs">3</span> Confirm
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form class="form-horizontal" novalidate="novalidate" name="adminCreate" action="{{ route('superadmin.administrator.admin.store') }}" method="POST">
                        @csrf
                        <div class="tab-content">
                            <div id="w1-account" class="tab-pane active">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w1-username">Username</label>
                                    <div class="col-sm-8">
                                        <input value="{{ old('username') }}" type="text" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" id="w1-username" required>

                                        @if ($errors->has('username'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w1-password">Password</label>
                                    <div class="col-sm-8">
                                        <input value="{{ old('password') }}" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="w1-password" minlength="8" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div id="w1-profile" class="tab-pane">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w1-first-name">First Name</label>
                                    <div class="col-sm-8">
                                        <input value="{{ old('first_name') }}" type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" id="w1-first-name" required>

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w1-last-name">Last Name</label>
                                    <div class="col-sm-8">
                                        <input value="{{ old('last_name') }}" type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" id="w1-last-name" required>

                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div id="w1-confirm" class="tab-pane">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="w1-email">Email</label>
                                    <div class="col-sm-8">
                                        <input value="{{ old('email') }}" type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="w1-email" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <div class="checkbox-custom">
                                            <input type="checkbox" name="terms" id="w1-terms" required>
                                            <label for="w1-terms">I'm Sure want to make Him/Her Admin</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <ul class="pager">
                        <li class="previous disabled">
                            <a><i class="fa fa-angle-left"></i> Previous</a>
                        </li>
                        <li class="finish hidden pull-right">
                            <a onclick='document.forms["adminCreate"].submit(); return false;'>Make Admin</a>
                        </li>
                        <li class="next">
                            <a>Next <i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
{{-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> --}}
<script src="{{ asset('backend/octopus/js/forms/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/octopus/js/forms/bootstrap.wizard.js') }}"></script>
<script src="{{ asset('backend/octopus/js/forms/examples.wizard.js') }}"></script>
@endsection
