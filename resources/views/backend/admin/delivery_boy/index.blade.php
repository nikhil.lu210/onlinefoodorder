@extends('layouts.backend.admin.app')

@section('page_title', '| All Delivery Boys')


@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}

tr>td{
    padding-top: 25px !important;
}
.avatar-td{
    max-width: 5% !important;
    padding: 5px 0px !important;
}
.avatar-td img.img-responsive.img-thumbnail{
    max-width: 70px !important;
    border-radius: 0px;
}
td.action-td {
    padding-top: 20px !important;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Delivery Boys</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Delivery Boys</span></li>
            <li><span>All Delivery Boys</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Admins</h2>
    </header>

    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>SL.</th>
                    <th class="text-center">Avatar</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($deliveryBoys as $key => $boy)
                    
                    <tr class="gradeX">
                        <td>  @if($key+1 < 10){{ 0 }}@endif{{ $key+1  }} </td>
                        <td class="avatar-td text-center">
                            <img src="{{ asset('storage/'.$boy->avatar) }}" alt="Image" class="img-responsive img-thumbnail">
                        </td>
                        <td>{{ $boy->name }}</td>
                        <td>{{ $boy->mobile }}</td>
                        <td>{{ $boy->email }}</td>
                        <td class="action-td">
                            <a href="{{ route('admin.delivery.destroy.delivery_boy', ['id' => $boy->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                            <a href="{{ route('admin.delivery.show.delivery_boy', ['id' => $boy->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


<script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>
@endsection
    