@extends('layouts.backend.admin.app')

@section('page_title', '| Add Delivery Boy')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Add New Delivery Boy</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Delivery Boys</span></li>
            <li><span>Add New Delivery Boy</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Add New Delivery Boy</h2>
    </header>
    
<form class="form-horizontal form-bordered" action="{{ route('admin.delivery.store.delivery_boy') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Avatar</label>
            <div class="col-md-8">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input">
                            <i class="fa fa-file fileupload-exists"></i>
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name="avatar"/>
                        </span>
                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="boyName">Delivery Boy Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="boyName" name="name">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="boyMobile">Mobile Number</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="boyMobile" name="mobile">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="boyEmail">Email Address</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="boyEmail" name="email">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label">Address</label>
            <div class="col-md-8">
                <textarea name="address" data-plugin-markdown-editor rows="5"></textarea>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label">Note</label>
            <div class="col-md-8">
                <textarea name="note" data-plugin-markdown-editor rows="5"></textarea>
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Add Delivery Boy</button>
    </div>
            
</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection
