@extends('layouts.backend.admin.app')

@section('page_title', '| Food Details')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}

.select2-container-active .select2-choice,
.select2-container-multi.select2-container-active .select2-choices,
.select2-drop-active {
	outline: none !important;
	-webkit-box-shadow: none !important;
	box-shadow: none !important;
	border-color: #fec62b;
}
.select2-results .select2-highlighted {
	color: #FFF;
	background-color: #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Food Name</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Food Menus</span></li>
            <li>
                <a href="{{ Route('admin.food.all.food_menu') }}">
                    <span>All Food Menus</span>
                </a>
            </li>
            <li><span>Food Name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Food Name</h2>
    </header>

<form class="form-horizontal form-bordered" action="{{ route('admin.food.update.food_menu', ['id' => $food->id]) }}" method="POST">
    @csrf
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label" for="foodName">Food Name</label>
            <div class="col-md-8">
            <input value="{{ $food->name }}" type="text" class="form-control" id="foodName" name="name">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="foodName">Price</label>
            <div class="col-md-8">
            <input value="{{ $food->price }}" type="number" step="0.01" class="form-control" id="price" name="price">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Food Category</label>
            <div class="col-md-8">
                <select data-plugin-selectTwo class="form-control populate" name="cat_id">

                    @if( !isset($categories[0]) )
                        <option value="">Add Category First</option>
                    @endif
                    @foreach ($categories as $category)

                        <option value="{{ $category->id }}" @if($category->id == $food->cat_id)selected @endif >{{ $category->name }}</option>
                    @endforeach

                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Included Items</label>
            <div class="col-md-8">
                <select multiple data-plugin-selectTwo class="form-control populate" name="extra_food_list[]">
                    @if( !isset($foods[0]) )
                        <option selected value="">Nothing Food Found for add Extra Food !!!</option>
                    @endif

                    <?php
                        $arr = (array) $food->extra_food_list;
                    ?>
                    @foreach ($foods as $food)
                        <option value="{{ $food->id }}" @if(in_array($food->id, $arr))selected @endif>{{ $food->name }}</option>
                    @endforeach

                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Note</label>
            <div class="col-md-8">
                <textarea name="note" data-plugin-markdown-editor rows="5"></textarea>
            </div>
        </div>
    </div>

    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Update</button>
    </div>

</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>

@endsection
