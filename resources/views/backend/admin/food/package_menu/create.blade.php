@extends('layouts.backend.admin.app')

@section('page_title', '| Add Package')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}

.select2-container-active .select2-choice,
.select2-container-multi.select2-container-active .select2-choices,
.select2-drop-active {
	outline: none !important;
	-webkit-box-shadow: none !important;
	box-shadow: none !important;
	border-color: #fec62b;
}
.select2-results .select2-highlighted {
	color: #FFF;
	background-color: #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Add Food Package</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Food Packages</span></li>
            <li><span>Add Food Package</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Add New Package</h2>
    </header>
    
<form class="form-horizontal form-bordered" action="{{ route('admin.food.store.food_package') }}" method="POST">
    @csrf
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label" for="packageName">Package Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="packageName" name="name" required>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label">Package Details</label>
            <div class="col-md-8">
                <textarea class="form-control {{ $errors->has('package_details') ? ' is-invalid' : '' }}" name="package_details" rows="2" required></textarea>
                @if ($errors->has('package_details'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('package_details') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="packageName">Price</label>
            <div class="col-md-8">
                <input type="number" step="0.01" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" id="price" name="price">
                @if ($errors->has('price'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Included Items</label>
            <div class="col-md-8">
                <select multiple data-plugin-selectTwo class="form-control populate  {{ $errors->has('extra_food_list') ? ' is-invalid' : '' }}" name="extra_food_list[]">
                    @if( !isset($foods[0]) )
                        <option selected value="">Nothing Food Found for add Extra Food !!!</option>
                    @endif

                    @foreach ($foods as $food)
                        <option value="{{ $food->id }}">{{ $food->name }}</option>    
                    @endforeach
                    
                </select>
                @if ($errors->has('extra_food_list'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('extra_food_list') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label">Note</label>
            <div class="col-md-8">
                <textarea name="note" data-plugin-markdown-editor rows="5"></textarea>
            </div>
        </div>
    </div>
    
    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Add Food</button>
    </div>
            
</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>

@endsection
