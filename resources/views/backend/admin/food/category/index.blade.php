@extends('layouts.backend.admin.app')

@section('page_title', '| Food Categories')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>All Food Categories</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Food Menus</span></li>
            <li><span>All Categories</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">    
        <h2 class="panel-title">All Categories</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Category Name</th>
                    <th>Total Food</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $key=>$category)
                    <tr class="gradeX">
                        <td> 
                            @if($key+1 < 10){{ 0 }}@endif{{ $key+1  }}
                        </td>
                        <td>{{ $category->name }}</td>
                        <td>{{ App\Model\Backend\Food\Food::where('cat_id', $category->id)->count() }}</td>
                        <td class="action-td">
                            <a href="{{ route('admin.category.destroy.category', ['id' => $category->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>

                            <a href="{{ route('admin.category.show.category', ['id' => $category->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


<script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>

@endsection