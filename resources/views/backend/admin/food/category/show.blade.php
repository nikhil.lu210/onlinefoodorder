@extends('layouts.backend.admin.app')

@section('page_title', '| Category Details')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
    .form-bordered .form-group{
        border-bottom: 0px solid #fff;
    }

    .md-editor.active{
        border-color: #fec62b;
        box-shadow: none !important;
        outline: none !important;
    }
    .md-editor.active textarea{
        border-top: 1px dashed #fec62b;
    }
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Category Name</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Food Menus</span></li>
            <li>
                <a href="{{ Route('admin.category.all.category') }}">
                    <span>All Categories</span>
                </a>
            </li>
            <li><span>Food Menus</span></li>
            <li><span>Category Name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Category Name</h2>
    </header>

<form class="form-horizontal form-bordered" action="{{ Route('admin.category.update.category', ['id' => $category->id]) }}" method="POST">
    @csrf

    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label" for="categoryName">Category Name</label>
            <div class="col-md-8">
            <input value="{{ $category->name }}" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="categoryName" name="name" required>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Note</label>
            <div class="col-md-8">
            <textarea name="note" data-plugin-markdown-editor rows="5" class="{{ $errors->has('note') ? ' is-invalid' : '' }}">{{ $category->note }}</textarea>
                @if ($errors->has('note'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('note') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Update Category</button>
    </div>

</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
@endsection
