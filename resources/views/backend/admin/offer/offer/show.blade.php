@extends('layouts.backend.admin.app')

@section('page_title', '| Offer Details')


@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
{{-- Date-Time Picker --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrapDatepickr-1.0.0.min.css') }}">
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}

.select2-container-active .select2-choice,
.select2-container-multi.select2-container-active .select2-choices,
.select2-drop-active {
	outline: none !important;
	-webkit-box-shadow: none !important;
	box-shadow: none !important;
	border-color: #fec62b;
}
.select2-results .select2-highlighted {
	color: #FFF;
	background-color: #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Offer Name</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Offers</span></li>
            <li>
                <a href="{{ Route('admin.offer.all.offer') }}">
                    <span>All Offers</span>
                </a>
            </li>
            <li><span>Offer Name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Offer Name</h2>
    </header>

<form class="form-horizontal form-bordered" action="{{ route('admin.offer.update.offer', ['id' => $offer->id]) }}" method="POST">
    @csrf
    <div class="panel-body">

        <div class="form-group">
            <label class="col-md-2 control-label" for="offerTitle">Offer Title</label>
            <div class="col-md-8">
                <input value="{{ $offer->name }}" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="offerTitle" name="name">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="discount">Discount <sup>(in %)</sup></label>
            <div class="col-md-8">
                <input value="{{ $offer->discount }}" type="number" class="form-control {{ $errors->has('discount') ? ' is-invalid' : '' }}" id="discount" name="discount" min="0" max="100">
                @if ($errors->has('discount'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('discount') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Included Items for Offer</label>
            <div class="col-md-8">
                <select multiple data-plugin-selectTwo class="form-control populate {{ $errors->has('food_ids') ? ' is-invalid' : '' }}" name="food_ids[]">
                    @if( !isset($foods[0]) )
                        <option selected value="">Nothing Food Found for add Extra Food !!!</option>
                    @endif
                    <?php
                        $arr = (array) $offer->food_ids;
                    ?>
                    @foreach ($foods as $food)
                        <option value="{{ $food->id }}" @if(in_array($food->id, $arr))selected @endif>{{ $food->name }}</option>
                    @endforeach

                </select>

                @if ($errors->has('food_ids'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('food_ids') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Included Packages for Offer</label>
            <div class="col-md-8">
                <select multiple data-plugin-selectTwo class="form-control populate {{ $errors->has('package_ids') ? ' is-invalid' : '' }}" name="package_ids[]">
                    @if( !isset($packages[0]) )
                        <option selected value="">Nothing Package Found for add Extra Package !!!</option>
                    @endif

                    <?php
                        $arr = (array) $offer->package_ids;
                    ?>
                    @foreach ($packages as $package)
                        <option value="{{ $package->id }}" @if(in_array($package->id, $arr))selected @endif>{{ $package->name }}</option>
                    @endforeach

                </select>


                @if ($errors->has('package_ids'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('package_ids') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="expDate">Expire Date</label>
            <div class="col-md-8">
                <input value="{{ $offer->exp_date }}" type="text" id="datePick" class="form-control {{ $errors->has('exp_date') ? ' is-invalid' : '' }}" id="expDate" name="exp_date">
                @if ($errors->has('exp_date'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('exp_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Note</label>
            <div class="col-md-8">
                <textarea class="{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" data-plugin-markdown-editor rows="5">{{ $offer->note }}</textarea>
                @if ($errors->has('note'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('note') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Update Offer</button>
    </div>

</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
<!-- Date-Time Picker -->
<script src="{{ asset('frontend/js/bootstrapDatepickr-1.0.0.min.js') }}"></script>

<script>
    // DatePicker
    $("#datePick").bootstrapDatepickr({
        date_format: "Y-m-d"
    });
</script>
@endsection
