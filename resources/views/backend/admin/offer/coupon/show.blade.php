@extends('layouts.backend.admin.app')

@section('page_title', '| Coupon Details')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
{{-- Date-Time Picker --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrapDatepickr-1.0.0.min.css') }}">
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
    .form-bordered .form-group{
        border-bottom: 0px solid #fff;
    }

    .md-editor.active{
        border-color: #fec62b;
        box-shadow: none !important;
        outline: none !important;
    }
    .md-editor.active textarea{
        border-top: 1px dashed #fec62b;
    }
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Coupon Title</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Offers</span></li>
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <span>All Coupons</span>
                </a>
            </li>
            <li><span>Coupon Title</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Coupon Title</h2>
    </header>

<form class="form-horizontal form-bordered" action="{{ route('admin.offer.update.coupon', ['id' => $coupon->id]) }}" method="POST">
    @csrf
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label" for="couponTitle">Coupon Title</label>
            <div class="col-md-8">
                <input value="{{ $coupon->name }}" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="couponTitle" name="name">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="couponCode">Coupon Code</label>
            <div class="col-md-8">
                <input value="{{ $coupon->code }}" type="text" class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}" id="couponCode" name="code">
                @if ($errors->has('code'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('code') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="couponDiscount">Total Discount</label>
            <div class="col-md-8">
                <input value="{{ $coupon->discount }}" type="number" step="0.01" class="form-control {{ $errors->has('discount') ? ' is-invalid' : '' }}" id="couponDiscount" name="discount" min="0" max="100">
                @if ($errors->has('discount'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('discount') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" min="0" max="10000" for="minPurchase">Minimum Purchase</label>
            <div class="col-md-8">
                <input value="{{ $coupon->min_purchase }}" type="number" step="0.01" class="form-control {{ $errors->has('min_purchase') ? ' is-invalid' : '' }}" id="minPurchase" name="min_purchase">
                @if ($errors->has('min_purchase'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('min_purchase') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="minPurchase">Uses Limit</label>
            <div class="col-md-8">
                <input type="number" value="{{$coupon->uses_limit}}" class="form-control {{ $errors->has('uses_limit') ? ' is-invalid' : '' }}" id="uses_limit" min="1" name="uses_limit">
                @if ($errors->has('uses_limit'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('uses_limit') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="expDate">Expire Date</label>
            <div class="col-md-8">
                <input value="{{ $coupon->exp_date }}" type="text" id="datePick" class="form-control {{ $errors->has('exp_date') ? ' is-invalid' : '' }}" id="expDate" name="exp_date">
                @if ($errors->has('exp_date'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('exp_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Note</label>
            <div class="col-md-8">
                <textarea class="{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" data-plugin-markdown-editor rows="5">{{ $coupon->note }}</textarea>
                @if ($errors->has('note'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('note') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Update Coupon</button>
    </div>

</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
<!-- Date-Time Picker -->
<script src="{{ asset('frontend/js/bootstrapDatepickr-1.0.0.min.js') }}"></script>

<script>
    // DatePicker
    $("#datePick").bootstrapDatepickr({
        date_format: "Y-m-d"
    });
</script>
@endsection
