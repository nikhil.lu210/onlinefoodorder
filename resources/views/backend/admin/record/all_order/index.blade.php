@extends('layouts.backend.admin.app')

@section('page_title', '| All Orders')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>All Orders</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Records</span></li>
            <li><span>All Orders</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        {{-- <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
        </div> --}}

        <h2 class="panel-title">All Orders</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $order)
                <tr class="gradeX">
                    <?php
                        // $date = date_create($post->updated_at);
                        $date = new DateTime($order->order_date);
                        $date->setTimezone(new DateTimeZone('GMT+06:00'));
                    ?>
                    <td>{{ $date->format('d-m-Y')}}</td>

                    <td>{{ $order->order_time }}</td>
                    <td>{{ $order->user->name }}</td>
                    <td>{{ $order->reduced_price }}</td>
                    <?php
                        switch ($order->order_status) {
                            case 0:
                                $status = "Pending";
                                break;
                            case 1:
                                $status = "Processing";
                                break;
                            case 2:
                                $status = "Completed";
                                break;
                            default:
                                $status = "Canceled";
                        }
                    ?>
                    <td>{{ $status }}</td>

                    <td class="action-td">
                        {{-- <a href="#" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a> --}}
                    <a href="{{ route('admin.record.all.order.show', ['id' => $order->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


<script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
<script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>
@endsection
