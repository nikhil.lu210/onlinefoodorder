@extends('layouts.backend.admin.app')

@section('page_title', '| Order Details')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}
.row.datatables-header.form-inline,
.row.datatables-footer{
    display: none;
}
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}


.select2-container-active .select2-choice,
.select2-container-multi.select2-container-active .select2-choices,
.select2-drop-active {
	outline: none !important;
	-webkit-box-shadow: none !important;
	box-shadow: none !important;
	border-color: #fec62b;
}
.select2-results .select2-highlighted {
	color: #FFF;
	background-color: #fec62b;
}

.order-process-div, .order-cancel-div{
    display: none;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>{{ $order->user->name }}</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Records</span></li>
            <li>
                <a href="{{ Route('admin.record.all.order') }}">
                    <span>All Orders</span>
                </a>
            </li>
            <li><span>{{ $order->user->name }}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{ $order->user->name }}</h2>
    </header>

    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none">
            <?php
                $order_date_time = new DateTime($order->updated_at);
                $delivery_date = new DateTime($order->order_date);
                $delivery_time = new DateTime($order->order_time);
                $order_date_time->setTimezone(new DateTimeZone('GMT+06:00'));
                $delivery_date->setTimezone(new DateTimeZone('GMT+06:00'));
                $delivery_time->setTimezone(new DateTimeZone('GMT+06:00'));
            ?>
            <tr>
                <th>Order date</th>
                <td>{{ $order_date_time->format('d-m-Y') }}</td>
            </tr>

            <tr>
                <th>Order Time</th>
                <td>{{ $order_date_time->format('h:i A') }}</td>
            </tr>

            <tr>
                <th>Order Type</th>
                <td>
                    @switch($order->order_type)
                        @case(1)
                            Pre-Order
                            @break
                        @case(2)
                            Post-Order
                            @break
                        @default
                    @endswitch
                </td>
            </tr>

            <tr>
                <th>Delivery Date</th>
                <td><b>{{ $delivery_date->format('d-m-Y') }}</b></td>
            </tr>

            <tr>
                <th>Delivery Time</th>
                <td><b>{{ $delivery_time->format('h:i A') }}</b></td>
            </tr>

            <tr>
                <th>Delivery Type</th>
                <td>
                    @switch($order->delivery_type)
                        @case(1)
                            Pickup
                            @break
                        @case(2)
                            Delivery
                            @break
                        @default
                            Not Defined
                    @endswitch
                </td>
            </tr>

            <tr>
                <th>Payment Type</th>
                <td>
                    @switch($order->payment_type)
                        @case(1)
                            Cash On Delivery
                            @break
                        @case(2)
                            Online Payment
                            @break
                        @default
                            Not Defined
                    @endswitch
                </td>
            </tr>

            <tr>
                <th>Order Status</th>
                <td>
                    @switch($order->order_status)
                        @case(-1)
                            Cancel
                            @break
                        @case(0)
                            Pending
                            @break
                        @case(1)
                            Processing
                            @break
                        @case(2)
                            Completed
                        @default
                            Not Defined
                    @endswitch
                </td>
            </tr>

            <tr>
                <th>Order List</th>
                <td>
                    <ul>
                        <?php $items = json_decode($order->order_list, true);?>
                        @foreach ($items as $item)
                        <li>
                            @if ($item["food_package"] == "food")
                                {{ App\Model\Backend\Food\Food::find($item["id"])->name }}
                            @else
                                {{ App\Model\Backend\Package\Package::find($item["id"])->name }}
                            @endif
                             ({{ $item["quantity"] }})
                        </li>
                        @if ($item["ex_quantity"])
                            <?php $arr = (array) $item["ex_quantity"]; ?>
                            @foreach($arr as $key => $ex_item)
                            <li>
                                {{ App\Model\Backend\Food\Food::find($key)->name }} ({{ $ex_item }})
                                </li>
                            @endforeach
                        @endif
                        @endforeach
                    </ul>
                </td>
            </tr>

            <tr>
                <th>Total Original Price</th>
                <td>{{ $order->original_price }}</td>
            </tr>

            <tr>
                <th>Total Discount</th>
                <td>{{ $order->total_discount }}</td>
            </tr>

            <tr>
                <th>Total Price</th>
                <td>{{ $order->reduced_price }}</td>
            </tr>

            <tr>
                <th>Applied Coupon</th>
                <?php
                    $coupon = App\Model\Backend\Coupon\Coupon::find($order->copon_id);
                ?>
                <td>@if($coupon){{ $coupon->name }}@else No coupon Used @endif</td>
            </tr>

            @if($order->order_status == 1 || $order->order_status == 2)
            <tr>
                <th>Assigned Delivery Boy</th>
                <td>@if($order->deliveryBoy){{ $order->deliveryBoy->name }}@else Not Assign @endif</td>
            </tr>
            @endif

            <tr>
                <th>Customer Name</th>
                <td>{{ $order->user->name }}</td>
            </tr>

            <tr>
                <th>Mobile Number</th>
                <td>{{ $order->phone }}</td>
            </tr>

            <tr>
                <th>Address</th>
                <td>
                    <ul>
                        <li>Country: {{ $order->country }}</li>
                        <li>City: {{ $order->city }}</li>
                        <li>Area: {{ $order->area }}</li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>

    <div class="panel-footer text-right">
        @if( $order->order_status == 0 )
            <button class="mb-xs mt-xs mr-xs btn btn-danger btn-cancel">Cancel Order</button>
            <button class="mb-xs mt-xs mr-xs btn btn-default btn-cancel-reset d-none">Reset Cancellation</button>
            <button class="mb-xs mt-xs mr-xs btn btn-success btn-process">Process Order</button>
            <button class="mb-xs mt-xs mr-xs btn btn-default btn-process-reset d-none">Reset Processing</button>
        @endif
        @if( $order->order_status == 1 )
            <a href="{{ route('admin.record.order.complete', ['id' => $order->id]) }}" class="mb-xs mt-xs mr-xs btn btn-success">Complete Order</a>
        @endif
    </div>
</section>
<!-- end: page -->


@if( $order->order_status == 0 )
<!-- Dellivery Boy Start -->
<section class="panel order-process-div" id="deliveryBoySection">
    <form action="{{ route('admin.record.order.confirm', ['id' => $order->id]) }}" method="post">
    @csrf
        <header class="panel-heading">
            <h2 class="panel-title">Dellivery Boy Assign</h2>
        </header>

        <div class="panel-body">
            <div class="form-group">
                <div class="col-md-12">
                    <label for="delivery_boy"><b>Select Delivery Boy</b></label>
                    <select data-plugin-selectTwo class="form-control populate" name="delivery_boy">
                        <option value="">Select Delivery Boy</option>
                        @foreach ($boys as $boy)
                            <option value="{{ $boy->id }}">{{ $boy->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="order_note"><b>Extra Note</b></label>
                    <textarea name="order_note" data-plugin-markdown-editor rows="5" placeholder="Extra Note"></textarea>
                </div>
            </div>
        </div>

        <div class="panel-footer text-right">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-success">Assign Dellivery Boy</button>
        </div>
    </form>
</section>
<!-- Dellivery Boy End -->



<!-- Cancel Order Start -->
<section class="panel order-cancel-div" id="orderCancelDiv">
    <form action="{{ route('admin.record.order.cancel', ['id' => $order->id]) }}" method="post">
    @csrf
        <header class="panel-heading">
            <h2 class="panel-title">Cancel Order</h2>
        </header>

        <div class="panel-body">
            <div class="form-group">
                <div class="col-md-12">
                    <label for="note"><b>Cancellation Reason</b></label>
                    <textarea name="cancel_note" data-plugin-markdown-editor rows="5" placeholder="Explain Cancellation Reason"></textarea>
                </div>
            </div>
        </div>

        <div class="panel-footer text-right">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-danger">Cancel Order</button>
        </div>
    </form>
</section>

@endif

<!-- Cancel Order End -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>

<script>
$(document).ready(function(){
    var cancel = $('.btn-cancel');
    var cancelReset = $('.btn-cancel-reset');
    var process = $('.btn-process');
    var processReset = $('.btn-process-reset');

    var processDiv = $('.order-process-div');
    var cancelDiv = $('.order-cancel-div');

    cancel.click(function(){
        cancelDiv.slideToggle();
        cancel.addClass('d-none');
        cancelReset.removeClass('d-none');
    });
    cancelReset.click(function(){
        cancelDiv.slideToggle();
        cancel.removeClass('d-none');
        cancelReset.addClass('d-none');
    });

    process.click(function(){
        processDiv.slideToggle();
        process.addClass('d-none');
        processReset.removeClass('d-none');
    });
    processReset.click(function(){
        processDiv.slideToggle();
        process.removeClass('d-none');
        processReset.addClass('d-none');
    });
});
</script>
@endsection
